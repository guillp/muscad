difference() {
  union() {
    // pulleys_holder
    difference() {
      // volume
      translate(v=[27.715, 0.0, 0.0]) 
      cube(size=[55.43, 38.36, 33.6], center=true);
      // front_top_chamfer
      translate(v=[-0.02, 17.18, 14.8]) 
      rotate(a=[90, 0, 0]) 
      rotate(a=[0, 90, 0]) 
      linear_extrude(height=55.47, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // front_bottom_chamfer
      translate(v=[-0.02, 17.18, -14.8]) 
      rotate(a=[0, 90, 0]) 
      linear_extrude(height=55.47, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // back_bottom_chamfer
      translate(v=[-0.02, -17.18, -14.8]) 
      rotate(a=[270, 0, 0]) 
      rotate(a=[0, 90, 0]) 
      linear_extrude(height=55.47, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // back_top_chamfer
      translate(v=[-0.02, -17.18, 14.8]) 
      rotate(a=[180, 0, 0]) 
      rotate(a=[0, 90, 0]) 
      linear_extrude(height=55.47, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
    }
    // body
    difference() {
      // volume
      translate(v=[18.025, 0.0, 0.0]) 
      cube(size=[36.05, 38.36, 62.4], center=true);
      // front_top_chamfer
      translate(v=[-0.02, 9.18, 21.2]) 
      rotate(a=[90, 0, 0]) 
      rotate(a=[0, 90, 0]) 
      linear_extrude(height=36.09, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[5.0, 5.0, 0.0]) 
        square(size=[10.04, 10.04], center=true);
        // fillet
        circle(d=20, $fn=157);
      }
      // front_bottom_chamfer
      translate(v=[-0.02, 9.18, -21.2]) 
      rotate(a=[0, 90, 0]) 
      linear_extrude(height=36.09, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[5.0, 5.0, 0.0]) 
        square(size=[10.04, 10.04], center=true);
        // fillet
        circle(d=20, $fn=157);
      }
    }
  }
  // fan_fix_clearance
  // volume
  translate(v=[46.24, 17.43, 8.91]) 
  cube(size=[20.38, 5.5, 15.82], center=true);
  // fan_fix_fillet
  translate(v=[91.48, 10.68, 12.8]) 
  rotate(a=[0, 270, 0]) 
  linear_extrude(height=55.43, center=false, convexity=10, twist=0.0, scale=1.0) 
  difference() {
    // box
    translate(v=[2.0, 2.0, 0.0]) 
    square(size=[4.04, 4.04], center=true);
    // fillet
    circle(d=8, $fn=62);
  }
  // belt_fix_clearance_front
  difference() {
    // volume
    translate(v=[25.25, 14.19, 7.0]) 
    cube(size=[20.0, 10.02, 12.0], center=true);
    // bottom_left_chamfer
    translate(v=[16.25, 19.22, 2.0]) 
    rotate(a=[90, 180, 0]) 
    linear_extrude(height=10.06, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[0.5, 0.5, 0.0]) 
      square(size=[1.04, 1.04], center=true);
      // fillet
      circle(d=2, $fn=15);
    }
    // top_left_chamfer
    translate(v=[16.25, 19.22, 12.0]) 
    rotate(a=[90, 270, 0]) 
    linear_extrude(height=10.06, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[0.5, 0.5, 0.0]) 
      square(size=[1.04, 1.04], center=true);
      // fillet
      circle(d=2, $fn=15);
    }
  }
  // belt_fix_clearance_back
  difference() {
    // volume
    translate(v=[25.25, -14.19, 7.0]) 
    cube(size=[20.0, 10.02, 12.0], center=true);
    // bottom_left_chamfer
    translate(v=[16.25, -9.16, 2.0]) 
    rotate(a=[90, 180, 0]) 
    linear_extrude(height=10.06, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[0.5, 0.5, 0.0]) 
      square(size=[1.04, 1.04], center=true);
      // fillet
      circle(d=2, $fn=15);
    }
    // top_left_chamfer
    translate(v=[16.25, -9.16, 12.0]) 
    rotate(a=[90, 270, 0]) 
    linear_extrude(height=10.06, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[0.5, 0.5, 0.0]) 
      square(size=[1.04, 1.04], center=true);
      // fillet
      circle(d=2, $fn=15);
    }
  }
  // y_fix_bolt
  translate(v=[22.25, -1.39, 7.0]) 
  rotate(a=[270, 0, 180]) 
  union() {
    // thread
    cylinder(h=25, d=3.38, $fn=26, center=true);
    // head
    translate(v=[0, 0, -13.88]) 
    cylinder(h=2.8, d=6.8, $fn=53, center=true);
    // head_clearance
    translate(v=[0, 0, -65.26]) 
    cylinder(h=100, d=6.8, $fn=53, center=true);
    translate(v=[0, 0, 11.18]) 
    // nut
    cylinder(h=2.8, d=6.8, $fn=6, center=true);
    // inline_nut_clearance
    hull() 
    {
      translate(v=[0, 0, 11.18]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
      translate(v=[0, 0, 31.18]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
    }
  }
  // x_belt_clearance
  // volume
  translate(v=[31.25, 0.0, -7.0]) 
  cube(size=[8.0, 40.36, 12.0], center=true);
  // y_bearing
  #rotate(a=[270, 0, 180]) 
  union() {
    // outer
    cylinder(h=30.6, d=21.2, $fn=166, center=true);
    // rod_clearance
    cylinder(h=70.6, d=14.4, $fn=113, center=true);
  }
  // x_rod_top
  #translate(v=[24.98, 0.0, 22.0]) 
  rotate(a=[270, 0, 90]) 
  // rod
  cylinder(h=50, d=8.4, $fn=65, center=true);
  // x_rod_bottom
  mirror(v=[0, 0, 1]) 
  // x_rod_top
  #translate(v=[24.98, 0.0, 22.0]) 
  rotate(a=[270, 0, 90]) 
  // rod
  cylinder(h=50, d=8.4, $fn=65, center=true);
  // front_x_pulley
  #translate(v=[46.25, 10.0, -6.35]) 
  union() {
    // body
    cylinder(h=10.7, d=18.4, $fn=144, center=true);
    // bolt
    translate(v=[0, 0, 4.39]) 
    union() {
      // thread
      cylinder(h=25, d=3.38, $fn=26, center=true);
      // head
      translate(v=[0, 0, -13.88]) 
      cylinder(h=2.8, d=6.8, $fn=53, center=true);
      // head_clearance
      translate(v=[0, 0, -65.26]) 
      cylinder(h=100, d=6.8, $fn=53, center=true);
      translate(v=[0, 0, 9.2]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
      // nut_clearance
      translate(v=[10.0, 0.0, 9.2]) 
      cube(size=[20, 6.089, 2.8], center=true);
    }
    // clearance
    rotate(a=[0, 0, 270]) 
    translate(v=[0.0, 10.0, 0.0]) 
    cube(size=[18.4, 20, 10.7], center=true);
    // clearance
    translate(v=[0.0, 10.0, 0.0]) 
    cube(size=[18.4, 20, 10.7], center=true);
  }
  // back_x_pulley
  mirror(v=[0, 1, 0]) 
  // front_x_pulley
  #translate(v=[46.25, 10.0, -6.35]) 
  union() {
    // body
    cylinder(h=10.7, d=18.4, $fn=144, center=true);
    // bolt
    translate(v=[0, 0, 4.39]) 
    union() {
      // thread
      cylinder(h=25, d=3.38, $fn=26, center=true);
      // head
      translate(v=[0, 0, -13.88]) 
      cylinder(h=2.8, d=6.8, $fn=53, center=true);
      // head_clearance
      translate(v=[0, 0, -65.26]) 
      cylinder(h=100, d=6.8, $fn=53, center=true);
      translate(v=[0, 0, 9.2]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
      // nut_clearance
      translate(v=[10.0, 0.0, 9.2]) 
      cube(size=[20, 6.089, 2.8], center=true);
    }
    // clearance
    rotate(a=[0, 0, 270]) 
    translate(v=[0.0, 10.0, 0.0]) 
    cube(size=[18.4, 20, 10.7], center=true);
    // clearance
    translate(v=[0.0, 10.0, 0.0]) 
    cube(size=[18.4, 20, 10.7], center=true);
  }
  // y_clamp_bolt_top_front
  translate(v=[2.0, 14.68, 13.0]) 
  rotate(a=[0, 90, 0]) 
  union() {
    // thread
    cylinder(h=16, d=3.38, $fn=26, center=true);
    // head
    translate(v=[0, 0, -9.38]) 
    cylinder(h=2.8, d=6.8, $fn=53, center=true);
    // head_clearance
    translate(v=[0, 0, -60.76]) 
    cylinder(h=100, d=6.8, $fn=53, center=true);
    rotate(a=[0, 0, 90]) 
    translate(v=[0, 0, 6.68]) 
    // nut
    cylinder(h=2.8, d=6.8, $fn=6, center=true);
    // nut_clearance
    rotate(a=[0, 0, 90]) 
    translate(v=[15.0, 0.0, 6.68]) 
    cube(size=[30, 6.089, 2.8], center=true);
  }
  // y_clamp_bolt_top_back
  mirror(v=[0, 1, 0]) 
  // y_clamp_bolt_top_front
  translate(v=[2.0, 14.68, 13.0]) 
  rotate(a=[0, 90, 0]) 
  union() {
    // thread
    cylinder(h=16, d=3.38, $fn=26, center=true);
    // head
    translate(v=[0, 0, -9.38]) 
    cylinder(h=2.8, d=6.8, $fn=53, center=true);
    // head_clearance
    translate(v=[0, 0, -60.76]) 
    cylinder(h=100, d=6.8, $fn=53, center=true);
    rotate(a=[0, 0, 90]) 
    translate(v=[0, 0, 6.68]) 
    // nut
    cylinder(h=2.8, d=6.8, $fn=6, center=true);
    // nut_clearance
    rotate(a=[0, 0, 90]) 
    translate(v=[15.0, 0.0, 6.68]) 
    cube(size=[30, 6.089, 2.8], center=true);
  }
  // y_clamp_bolt_bottom_front
  mirror(v=[0, 0, 1]) 
  // y_clamp_bolt_top_front
  translate(v=[2.0, 14.68, 13.0]) 
  rotate(a=[0, 90, 0]) 
  union() {
    // thread
    cylinder(h=16, d=3.38, $fn=26, center=true);
    // head
    translate(v=[0, 0, -9.38]) 
    cylinder(h=2.8, d=6.8, $fn=53, center=true);
    // head_clearance
    translate(v=[0, 0, -60.76]) 
    cylinder(h=100, d=6.8, $fn=53, center=true);
    rotate(a=[0, 0, 90]) 
    translate(v=[0, 0, 6.68]) 
    // nut
    cylinder(h=2.8, d=6.8, $fn=6, center=true);
    // nut_clearance
    rotate(a=[0, 0, 90]) 
    translate(v=[15.0, 0.0, 6.68]) 
    cube(size=[30, 6.089, 2.8], center=true);
  }
  // y_clamp_bolt_bottom_back
  mirror(v=[0, 0, 1]) 
  // y_clamp_bolt_top_back
  mirror(v=[0, 1, 0]) 
  // y_clamp_bolt_top_front
  translate(v=[2.0, 14.68, 13.0]) 
  rotate(a=[0, 90, 0]) 
  union() {
    // thread
    cylinder(h=16, d=3.38, $fn=26, center=true);
    // head
    translate(v=[0, 0, -9.38]) 
    cylinder(h=2.8, d=6.8, $fn=53, center=true);
    // head_clearance
    translate(v=[0, 0, -60.76]) 
    cylinder(h=100, d=6.8, $fn=53, center=true);
    rotate(a=[0, 0, 90]) 
    translate(v=[0, 0, 6.68]) 
    // nut
    cylinder(h=2.8, d=6.8, $fn=6, center=true);
    // nut_clearance
    rotate(a=[0, 0, 90]) 
    translate(v=[15.0, 0.0, 6.68]) 
    cube(size=[30, 6.089, 2.8], center=true);
  }
  // clamp_clearance_bottom
  translate(v=[18.025, -19.18, -25.45]) 
  cube(size=[36.09, 38.36, 1.5], center=true);
  // clamp_clearance_up
  mirror(v=[0, 0, 1]) 
  // clamp_clearance_bottom
  translate(v=[18.025, -19.18, -25.45]) 
  cube(size=[36.09, 38.36, 1.5], center=true);
  // clamp_bolt_up
  translate(v=[18.025, -10.18, 22.42]) 
  rotate(a=[180, 0, 0]) 
  union() {
    // thread
    cylinder(h=12, d=3.38, $fn=26, center=true);
    // head
    translate(v=[0, 0, -7.38]) 
    cylinder(h=2.8, d=6.8, $fn=53, center=true);
    // head_clearance
    translate(v=[0, 0, -58.76]) 
    cylinder(h=100, d=6.8, $fn=53, center=true);
    rotate(a=[0, 0, 90]) 
    translate(v=[0, 0, 4.68]) 
    // nut
    cylinder(h=2.8, d=6.8, $fn=6, center=true);
    // nut_clearance
    rotate(a=[0, 0, 90]) 
    translate(v=[15.0, 0.0, 4.68]) 
    cube(size=[30, 6.089, 2.8], center=true);
  }
  // clamp_bolt_down
  mirror(v=[0, 0, 1]) 
  // clamp_bolt_up
  translate(v=[18.025, -10.18, 22.42]) 
  rotate(a=[180, 0, 0]) 
  union() {
    // thread
    cylinder(h=12, d=3.38, $fn=26, center=true);
    // head
    translate(v=[0, 0, -7.38]) 
    cylinder(h=2.8, d=6.8, $fn=53, center=true);
    // head_clearance
    translate(v=[0, 0, -58.76]) 
    cylinder(h=100, d=6.8, $fn=53, center=true);
    rotate(a=[0, 0, 90]) 
    translate(v=[0, 0, 4.68]) 
    // nut
    cylinder(h=2.8, d=6.8, $fn=6, center=true);
    // nut_clearance
    rotate(a=[0, 0, 90]) 
    translate(v=[15.0, 0.0, 4.68]) 
    cube(size=[30, 6.089, 2.8], center=true);
  }
  // y_belt_outer_clearance
  // volume
  translate(v=[31.25, 0.0, 7.0]) 
  cube(size=[8.0, 40.36, 12.0], center=true);
  // y_belt_inner_clearance
  // volume
  translate(v=[41.25, 0.0, 7.0]) 
  cube(size=[8.0, 40.36, 12.0], center=true);
}