union() {
  difference() {
    union() {
      // body
      difference() {
        // volume
        translate(v=[0.0, 15.17, 0.0]) 
        cube(size=[61.56, 8.0, 78.26], center=true);
        // top_right_chamfer
        translate(v=[28.78, 15.17, 37.13]) 
        rotate(a=[90, 0, 0]) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          cube(size=[2.04, 2.04, 8.04], center=true);
          // fillet
          cylinder(h=10.04, d=4, $fn=31, center=true);
        }
        // bottom_right_chamfer
        translate(v=[28.78, 15.17, -37.13]) 
        rotate(a=[0, 90, 0]) 
        rotate(a=[90, 0, 0]) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          cube(size=[2.04, 2.04, 8.04], center=true);
          // fillet
          cylinder(h=10.04, d=4, $fn=31, center=true);
        }
        // bottom_left_chamfer
        translate(v=[-28.78, 15.17, -37.13]) 
        rotate(a=[0, 180, 0]) 
        rotate(a=[90, 0, 0]) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          cube(size=[2.04, 2.04, 8.04], center=true);
          // fillet
          cylinder(h=10.04, d=4, $fn=31, center=true);
        }
        // top_left_chamfer
        translate(v=[-28.78, 15.17, 37.13]) 
        rotate(a=[0, 270, 0]) 
        rotate(a=[90, 0, 0]) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          cube(size=[2.04, 2.04, 8.04], center=true);
          // fillet
          cylinder(h=10.04, d=4, $fn=31, center=true);
        }
      }
      // extruder_holder
      difference() {
        // volume
        translate(v=[0.0, 32.67, 0.0]) 
        cube(size=[40.0, 27.0, 11.0], center=true);
        // top_right_chamfer
        translate(v=[18.0, 32.67, 3.5]) 
        rotate(a=[90, 0, 0]) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          cube(size=[2.04, 2.04, 27.04], center=true);
          // fillet
          cylinder(h=29.04, d=4, $fn=31, center=true);
        }
        // bottom_right_chamfer
        translate(v=[18.0, 32.67, -3.5]) 
        rotate(a=[0, 90, 0]) 
        rotate(a=[90, 0, 0]) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          cube(size=[2.04, 2.04, 27.04], center=true);
          // fillet
          cylinder(h=29.04, d=4, $fn=31, center=true);
        }
        // bottom_left_chamfer
        translate(v=[-18.0, 32.67, -3.5]) 
        rotate(a=[0, 180, 0]) 
        rotate(a=[90, 0, 0]) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          cube(size=[2.04, 2.04, 27.04], center=true);
          // fillet
          cylinder(h=29.04, d=4, $fn=31, center=true);
        }
        // top_left_chamfer
        translate(v=[-18.0, 32.67, 3.5]) 
        rotate(a=[0, 270, 0]) 
        rotate(a=[90, 0, 0]) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          cube(size=[2.04, 2.04, 27.04], center=true);
          // fillet
          cylinder(h=29.04, d=4, $fn=31, center=true);
        }
      }
      // left_blower_holder
      difference() {
        // volume
        translate(v=[-35.3595, 17.17, 11.4]) 
        cube(size=[13.159, 4.0, 10.5], center=true);
        // bottom_left_chamfer
        translate(v=[-35.939, 17.17, 12.15]) 
        rotate(a=[0, 180, 0]) 
        rotate(a=[90, 0, 0]) 
        difference() {
          // box
          translate(v=[3.0, 3.0, 0.0]) 
          cube(size=[6.04, 6.04, 4.04], center=true);
          // fillet
          cylinder(h=6.04, d=12, $fn=94, center=true);
        }
        // top_left_chamfer
        translate(v=[-35.939, 17.17, 10.65]) 
        rotate(a=[0, 270, 0]) 
        rotate(a=[90, 0, 0]) 
        difference() {
          // box
          translate(v=[3.0, 3.0, 0.0]) 
          cube(size=[6.04, 6.04, 4.04], center=true);
          // fillet
          cylinder(h=6.04, d=12, $fn=94, center=true);
        }
      }
      // cable_holder
      difference() {
        // volume
        translate(v=[10.811, 15.16, 64.11]) 
        cube(size=[22.5, 8.02, 50.0], center=true);
        // top_right_chamfer
        translate(v=[16.061, 15.16, 83.11]) 
        rotate(a=[90, 0, 0]) 
        difference() {
          // box
          translate(v=[3.0, 3.0, 0.0]) 
          cube(size=[6.04, 6.04, 8.06], center=true);
          // fillet
          cylinder(h=10.06, d=12, $fn=94, center=true);
        }
        // top_left_chamfer
        translate(v=[5.561, 15.16, 83.11]) 
        rotate(a=[0, 270, 0]) 
        rotate(a=[90, 0, 0]) 
        difference() {
          // box
          translate(v=[3.0, 3.0, 0.0]) 
          cube(size=[6.04, 6.04, 8.06], center=true);
          // fillet
          cylinder(h=10.06, d=12, $fn=94, center=true);
        }
      }
    }
    // x_top_rod
    translate(v=[0.0, 0, 22.0]) 
    rotate(a=[0, 270, 0]) 
    // rod
    cylinder(h=100, d=8.4, $fn=65, center=true);
    // x_bottom_rod
    mirror(v=[0, 0, 1]) 
    // x_top_rod
    translate(v=[0.0, 0, 22.0]) 
    rotate(a=[0, 270, 0]) 
    // rod
    cylinder(h=100, d=8.4, $fn=65, center=true);
    // x_bearing_top_right
    translate(v=[15.65, 0.0, 22.0]) 
    rotate(a=[90, 0, 0]) 
    rotate(a=[0, 0, 90]) 
    union() {
      // base
      // volume
      translate(v=[0.0, 0.0, 0.0]) 
      cube(size=[34.3, 30.3, 22.3], center=true);
      // bolts
      union() {
        translate(v=[-12.0, -9.0, -8.15]) 
        union() {
          // thread
          cylinder(h=10, d=4.278, $fn=33, center=true);
          // head
          translate(v=[0, 0, -6.73]) 
          cylinder(h=3.5, d=8.4, $fn=65, center=true);
          // head_clearance
          translate(v=[0, 0, -58.46]) 
          cylinder(h=100, d=8.4, $fn=65, center=true);
        }
        translate(v=[-12.0, 9.0, -8.15]) 
        union() {
          // thread
          cylinder(h=10, d=4.278, $fn=33, center=true);
          // head
          translate(v=[0, 0, -6.73]) 
          cylinder(h=3.5, d=8.4, $fn=65, center=true);
          // head_clearance
          translate(v=[0, 0, -58.46]) 
          cylinder(h=100, d=8.4, $fn=65, center=true);
        }
        translate(v=[12.0, -9.0, -8.15]) 
        union() {
          // thread
          cylinder(h=10, d=4.278, $fn=33, center=true);
          // head
          translate(v=[0, 0, -6.73]) 
          cylinder(h=3.5, d=8.4, $fn=65, center=true);
          // head_clearance
          translate(v=[0, 0, -58.46]) 
          cylinder(h=100, d=8.4, $fn=65, center=true);
        }
        translate(v=[12.0, 9.0, -8.15]) 
        union() {
          // thread
          cylinder(h=10, d=4.278, $fn=33, center=true);
          // head
          translate(v=[0, 0, -6.73]) 
          cylinder(h=3.5, d=8.4, $fn=65, center=true);
          // head_clearance
          translate(v=[0, 0, -58.46]) 
          cylinder(h=100, d=8.4, $fn=65, center=true);
        }
      }
      // rod_clearance
      rotate(a=[90, 0, 0]) 
      hull() 
      {
        cylinder(h=234.3, d=10, $fn=78, center=true);
        translate(v=[0, 10, 0]) 
        cylinder(h=234.3, d=10, $fn=78, center=true);
      }
    }
    // x_bearing_bottom
    translate(v=[0, 0.0, -22.0]) 
    rotate(a=[90, 0, 0]) 
    rotate(a=[0, 0, 90]) 
    union() {
      // base
      // volume
      translate(v=[0.0, 0.0, 0.0]) 
      cube(size=[34.3, 30.3, 22.3], center=true);
      // bolts
      union() {
        translate(v=[-12.0, -9.0, -8.15]) 
        union() {
          // thread
          cylinder(h=10, d=4.278, $fn=33, center=true);
          // head
          translate(v=[0, 0, -6.73]) 
          cylinder(h=3.5, d=8.4, $fn=65, center=true);
          // head_clearance
          translate(v=[0, 0, -58.46]) 
          cylinder(h=100, d=8.4, $fn=65, center=true);
        }
        translate(v=[-12.0, 9.0, -8.15]) 
        union() {
          // thread
          cylinder(h=10, d=4.278, $fn=33, center=true);
          // head
          translate(v=[0, 0, -6.73]) 
          cylinder(h=3.5, d=8.4, $fn=65, center=true);
          // head_clearance
          translate(v=[0, 0, -58.46]) 
          cylinder(h=100, d=8.4, $fn=65, center=true);
        }
        translate(v=[12.0, -9.0, -8.15]) 
        union() {
          // thread
          cylinder(h=10, d=4.278, $fn=33, center=true);
          // head
          translate(v=[0, 0, -6.73]) 
          cylinder(h=3.5, d=8.4, $fn=65, center=true);
          // head_clearance
          translate(v=[0, 0, -58.46]) 
          cylinder(h=100, d=8.4, $fn=65, center=true);
        }
        translate(v=[12.0, 9.0, -8.15]) 
        union() {
          // thread
          cylinder(h=10, d=4.278, $fn=33, center=true);
          // head
          translate(v=[0, 0, -6.73]) 
          cylinder(h=3.5, d=8.4, $fn=65, center=true);
          // head_clearance
          translate(v=[0, 0, -58.46]) 
          cylinder(h=100, d=8.4, $fn=65, center=true);
        }
      }
      // rod_clearance
      rotate(a=[90, 0, 0]) 
      hull() 
      {
        cylinder(h=234.3, d=10, $fn=78, center=true);
        translate(v=[0, 0, 0]) 
        cylinder(h=234.3, d=10, $fn=78, center=true);
      }
    }
    // x_bearing_top_left
    mirror(v=[1, 0, 0]) 
    // x_bearing_top_right
    translate(v=[15.65, 0.0, 22.0]) 
    rotate(a=[90, 0, 0]) 
    rotate(a=[0, 0, 90]) 
    union() {
      // base
      // volume
      translate(v=[0.0, 0.0, 0.0]) 
      cube(size=[34.3, 30.3, 22.3], center=true);
      // bolts
      union() {
        translate(v=[-12.0, -9.0, -8.15]) 
        union() {
          // thread
          cylinder(h=10, d=4.278, $fn=33, center=true);
          // head
          translate(v=[0, 0, -6.73]) 
          cylinder(h=3.5, d=8.4, $fn=65, center=true);
          // head_clearance
          translate(v=[0, 0, -58.46]) 
          cylinder(h=100, d=8.4, $fn=65, center=true);
        }
        translate(v=[-12.0, 9.0, -8.15]) 
        union() {
          // thread
          cylinder(h=10, d=4.278, $fn=33, center=true);
          // head
          translate(v=[0, 0, -6.73]) 
          cylinder(h=3.5, d=8.4, $fn=65, center=true);
          // head_clearance
          translate(v=[0, 0, -58.46]) 
          cylinder(h=100, d=8.4, $fn=65, center=true);
        }
        translate(v=[12.0, -9.0, -8.15]) 
        union() {
          // thread
          cylinder(h=10, d=4.278, $fn=33, center=true);
          // head
          translate(v=[0, 0, -6.73]) 
          cylinder(h=3.5, d=8.4, $fn=65, center=true);
          // head_clearance
          translate(v=[0, 0, -58.46]) 
          cylinder(h=100, d=8.4, $fn=65, center=true);
        }
        translate(v=[12.0, 9.0, -8.15]) 
        union() {
          // thread
          cylinder(h=10, d=4.278, $fn=33, center=true);
          // head
          translate(v=[0, 0, -6.73]) 
          cylinder(h=3.5, d=8.4, $fn=65, center=true);
          // head_clearance
          translate(v=[0, 0, -58.46]) 
          cylinder(h=100, d=8.4, $fn=65, center=true);
        }
      }
      // rod_clearance
      rotate(a=[90, 0, 0]) 
      hull() 
      {
        cylinder(h=234.3, d=10, $fn=78, center=true);
        translate(v=[0, 10, 0]) 
        cylinder(h=234.3, d=10, $fn=78, center=true);
      }
    }
    // center_pulleys_bolt
    translate(v=[0, 10.17, 0.0]) 
    rotate(a=[270, 0, 0]) 
    union() {
      // thread
      cylinder(h=16, d=3.38, $fn=26, center=true);
      // head
      translate(v=[0, 0, -9.38]) 
      cylinder(h=2.8, d=6.8, $fn=53, center=true);
      // head_clearance
      translate(v=[0, 0, -60.76]) 
      cylinder(h=100, d=6.8, $fn=53, center=true);
      translate(v=[0, 0, 6.68]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
      // inline_nut_clearance
      hull() 
      {
        translate(v=[0, 0, 6.68]) 
        // nut
        cylinder(h=2.8, d=6.8, $fn=6, center=true);
        translate(v=[0, 0, 16.68]) 
        // nut
        cylinder(h=2.8, d=6.8, $fn=6, center=true);
      }
    }
    // extruder
    #translate(v=[0.0, 46.995, -54.31]) 
    // extruder
    union() {
      translate(v=[0, 0, 0.99]) 
      cylinder(h=2, d1=1, d2=6, $fn=7, center=true);
      translate(v=[0, 0, 2.98]) 
      cylinder(h=2, d=8.06, $fn=6, center=true);
      translate(v=[0, 0, 4.97]) 
      cylinder(h=2, d=5, $fn=39, center=true);
      translate(v=[0, 0, 5.96]) 
      // volume
      translate(v=[0.0, 5.5, 5.25]) 
      cube(size=[16, 20.0, 10.5], center=true);
      translate(v=[0, 0, 18.0]) 
      cylinder(h=3.1, d=4, $fn=31, center=true);
      translate(v=[0, 0, 32.04]) 
      cylinder(h=25, d=22.3, $fn=175, center=true);
      translate(v=[0, 0, 48.03]) 
      cylinder(h=7, d=16, $fn=125, center=true);
      translate(v=[0, 0, 54.42]) 
      cylinder(h=5.8, d=12, $fn=94, center=true);
      translate(v=[0, 0, 60.81]) 
      cylinder(h=7, d=16, $fn=125, center=true);
    }
    // blower
    #translate(v=[-17.189, 26.97, 30.7]) 
    rotate(a=[90, 0, 0]) 
    rotate(a=[0, 0, 180]) 
    union() {
      // fan
      cylinder(h=15.4, d=50.4, $fn=395, center=true);
      // bolt_holders
      hull() 
      {
        translate(v=[-23.0, -19.0, 0.0]) 
        cylinder(h=15.4, d=6.378, $fn=50, center=true);
        translate(v=[19.5, 19.3, 0.0]) 
        cylinder(h=15.4, d=6.378, $fn=50, center=true);
      }
      // blower
      // volume
      translate(v=[-17.0, 22.6, 0.0]) 
      cube(size=[19.4, 45.2, 15.4], center=true);
      // back_bolt
      translate(v=[-23.0, -19.0, 1.88]) 
      union() {
        // thread
        cylinder(h=20, d=4.378, $fn=34, center=true);
        // head
        translate(v=[0, 0, -11.78]) 
        cylinder(h=3.6, d=8.5, $fn=66, center=true);
        // head_clearance
        translate(v=[0, 0, -63.56]) 
        cylinder(h=100, d=8.5, $fn=66, center=true);
        translate(v=[0, 0, 8.28]) 
        // nut
        cylinder(h=3.6, d=8.5, $fn=6, center=true);
        // inline_nut_clearance
        hull() 
        {
          translate(v=[0, 0, 8.28]) 
          // nut
          cylinder(h=3.6, d=8.5, $fn=6, center=true);
          translate(v=[0, 0, 28.28]) 
          // nut
          cylinder(h=3.6, d=8.5, $fn=6, center=true);
        }
      }
      // front_bolt
      translate(v=[19.5, 19.3, 1.88]) 
      union() {
        // thread
        cylinder(h=20, d=4.378, $fn=34, center=true);
        // head
        translate(v=[0, 0, -11.78]) 
        cylinder(h=3.6, d=8.5, $fn=66, center=true);
        // head_clearance
        translate(v=[0, 0, -63.56]) 
        cylinder(h=100, d=8.5, $fn=66, center=true);
        translate(v=[0, 0, 8.28]) 
        // nut
        cylinder(h=3.6, d=8.5, $fn=6, center=true);
        // inline_nut_clearance
        hull() 
        {
          translate(v=[0, 0, 8.28]) 
          // nut
          cylinder(h=3.6, d=8.5, $fn=6, center=true);
          translate(v=[0, 0, 28.28]) 
          // nut
          cylinder(h=3.6, d=8.5, $fn=6, center=true);
        }
      }
    }
    // cable_hole_bottom_left
    difference() {
      // volume
      translate(v=[25.8, 15.17, -31.13]) 
      cube(size=[10.0, 10.0, 10.0], center=true);
      // bottom_left_chamfer
      translate(v=[22.8, 15.17, -34.13]) 
      rotate(a=[0, 180, 0]) 
      rotate(a=[90, 0, 0]) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        cube(size=[2.04, 2.04, 10.04], center=true);
        // fillet
        cylinder(h=12.04, d=4, $fn=31, center=true);
      }
      // top_left_chamfer
      translate(v=[22.8, 15.17, -28.13]) 
      rotate(a=[0, 270, 0]) 
      rotate(a=[90, 0, 0]) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        cube(size=[2.04, 2.04, 10.04], center=true);
        // fillet
        cylinder(h=12.04, d=4, $fn=31, center=true);
      }
      // top_right_chamfer
      translate(v=[28.82, 15.17, -28.11]) 
      rotate(a=[90, 0, 0]) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        cube(size=[2, 2, 10.04], center=true);
        // chamfer
        rotate(a=[0, 0, 45]) 
        cube(size=[2.8284, 2.8284, 12.04], center=true);
      }
      // bottom_right_chamfer
      translate(v=[28.82, 15.17, -34.15]) 
      rotate(a=[0, 90, 0]) 
      rotate(a=[90, 0, 0]) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        cube(size=[2, 2, 10.04], center=true);
        // chamfer
        rotate(a=[0, 0, 45]) 
        cube(size=[2.8284, 2.8284, 12.04], center=true);
      }
    }
    // cable_hole_bottom_right
    mirror(v=[1, 0, 0]) 
    // cable_hole_bottom_left
    difference() {
      // volume
      translate(v=[25.8, 15.17, -31.13]) 
      cube(size=[10.0, 10.0, 10.0], center=true);
      // bottom_left_chamfer
      translate(v=[22.8, 15.17, -34.13]) 
      rotate(a=[0, 180, 0]) 
      rotate(a=[90, 0, 0]) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        cube(size=[2.04, 2.04, 10.04], center=true);
        // fillet
        cylinder(h=12.04, d=4, $fn=31, center=true);
      }
      // top_left_chamfer
      translate(v=[22.8, 15.17, -28.13]) 
      rotate(a=[0, 270, 0]) 
      rotate(a=[90, 0, 0]) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        cube(size=[2.04, 2.04, 10.04], center=true);
        // fillet
        cylinder(h=12.04, d=4, $fn=31, center=true);
      }
      // top_right_chamfer
      translate(v=[28.82, 15.17, -28.11]) 
      rotate(a=[90, 0, 0]) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        cube(size=[2, 2, 10.04], center=true);
        // chamfer
        rotate(a=[0, 0, 45]) 
        cube(size=[2.8284, 2.8284, 12.04], center=true);
      }
      // bottom_right_chamfer
      translate(v=[28.82, 15.17, -34.15]) 
      rotate(a=[0, 90, 0]) 
      rotate(a=[90, 0, 0]) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        cube(size=[2, 2, 10.04], center=true);
        // chamfer
        rotate(a=[0, 0, 45]) 
        cube(size=[2.8284, 2.8284, 12.04], center=true);
      }
    }
    // fan
    translate(v=[0.0, 69.37, -14.7]) 
    rotate(a=[90, 0, 0]) 
    union() {
      // body
      difference() {
        // volume
        translate(v=[0.0, 0.0, 0.0]) 
        cube(size=[40.4, 40.4, 20.4], center=true);
        // front_right_chamfer
        translate(v=[18.2, 18.2, 0.0]) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          cube(size=[2.04, 2.04, 20.44], center=true);
          // fillet
          cylinder(h=22.44, d=4, $fn=31, center=true);
        }
        // back_right_chamfer
        translate(v=[18.2, -18.2, 0.0]) 
        rotate(a=[0, 0, 270]) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          cube(size=[2.04, 2.04, 20.44], center=true);
          // fillet
          cylinder(h=22.44, d=4, $fn=31, center=true);
        }
        // back_left_chamfer
        translate(v=[-18.2, -18.2, 0.0]) 
        rotate(a=[0, 0, 180]) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          cube(size=[2.04, 2.04, 20.44], center=true);
          // fillet
          cylinder(h=22.44, d=4, $fn=31, center=true);
        }
        // front_left_chamfer
        translate(v=[-18.2, 18.2, 0.0]) 
        rotate(a=[0, 0, 90]) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          cube(size=[2.04, 2.04, 20.44], center=true);
          // fillet
          cylinder(h=22.44, d=4, $fn=31, center=true);
        }
      }
      // bolts
      translate(v=[0, 0, 5.08]) 
      {
        rotate(a=[0, 0, 225]) 
        translate(v=[22.6274, 0, 0]) 
        union() {
          // thread
          cylinder(h=25, d=3.38, $fn=26, center=true);
          // head
          translate(v=[0, 0, -13.88]) 
          cylinder(h=2.8, d=6.8, $fn=53, center=true);
          // head_clearance
          translate(v=[0, 0, -65.26]) 
          cylinder(h=100, d=6.8, $fn=53, center=true);
          translate(v=[0, 0, 11.18]) 
          // nut
          cylinder(h=2.8, d=6.8, $fn=6, center=true);
        }
        rotate(a=[0, 0, 315]) 
        translate(v=[22.6274, 0, 0]) 
        union() {
          // thread
          cylinder(h=25, d=3.38, $fn=26, center=true);
          // head
          translate(v=[0, 0, -13.88]) 
          cylinder(h=2.8, d=6.8, $fn=53, center=true);
          // head_clearance
          translate(v=[0, 0, -65.26]) 
          cylinder(h=100, d=6.8, $fn=53, center=true);
          translate(v=[0, 0, 11.18]) 
          // nut
          cylinder(h=2.8, d=6.8, $fn=6, center=true);
        }
      }
      // bolts
      translate(v=[0, 0, 12.58]) 
      {
        rotate(a=[0, 0, 45]) 
        translate(v=[22.6274, 0, 0]) 
        union() {
          // thread
          cylinder(h=40, d=3.38, $fn=26, center=true);
          // head
          translate(v=[0, 0, -21.38]) 
          cylinder(h=2.8, d=6.8, $fn=53, center=true);
          // head_clearance
          translate(v=[0, 0, -72.76]) 
          cylinder(h=100, d=6.8, $fn=53, center=true);
        }
        rotate(a=[0, 0, 135]) 
        translate(v=[22.6274, 0, 0]) 
        union() {
          // thread
          cylinder(h=40, d=3.38, $fn=26, center=true);
          // head
          translate(v=[0, 0, -21.38]) 
          cylinder(h=2.8, d=6.8, $fn=53, center=true);
          // head_clearance
          translate(v=[0, 0, -72.76]) 
          cylinder(h=100, d=6.8, $fn=53, center=true);
        }
      }
    }
    // extruder_clamp_bolt_right
    translate(v=[-16.0, 44.78, 1.3]) 
    rotate(a=[90, 0, 0]) 
    union() {
      // thread
      cylinder(h=12, d=3.38, $fn=26, center=true);
      // head
      translate(v=[0, 0, -7.38]) 
      cylinder(h=2.8, d=6.8, $fn=53, center=true);
      // head_clearance
      translate(v=[0, 0, -58.76]) 
      cylinder(h=100, d=6.8, $fn=53, center=true);
      rotate(a=[0, 0, 90]) 
      translate(v=[0, 0, 4.68]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
      // nut_clearance
      rotate(a=[0, 0, 90]) 
      translate(v=[10.0, 0.0, 4.68]) 
      cube(size=[20, 6.089, 2.8], center=true);
    }
    // extruder_clamp_bolt_left
    mirror(v=[1, 0, 0]) 
    // extruder_clamp_bolt_right
    translate(v=[-16.0, 44.78, 1.3]) 
    rotate(a=[90, 0, 0]) 
    union() {
      // thread
      cylinder(h=12, d=3.38, $fn=26, center=true);
      // head
      translate(v=[0, 0, -7.38]) 
      cylinder(h=2.8, d=6.8, $fn=53, center=true);
      // head_clearance
      translate(v=[0, 0, -58.76]) 
      cylinder(h=100, d=6.8, $fn=53, center=true);
      rotate(a=[0, 0, 90]) 
      translate(v=[0, 0, 4.68]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
      // nut_clearance
      rotate(a=[0, 0, 90]) 
      translate(v=[10.0, 0.0, 4.68]) 
      cube(size=[20, 6.089, 2.8], center=true);
    }
  }
  difference() {
    // body
    difference() {
      // volume
      translate(v=[0.0, 0.685, -5.64]) 
      cube(size=[61.56, 20.77, 20.78], center=true);
      // back_bottom_chamfer
      translate(v=[0.0, -7.7, -14.03]) 
      rotate(a=[270, 0, 0]) 
      rotate(a=[0, 90, 0]) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        cube(size=[2.04, 2.04, 61.6], center=true);
        // fillet
        cylinder(h=63.6, d=4, $fn=31, center=true);
      }
    }
    // center_pulleys_bolt
    translate(v=[0, 10.17, 0.0]) 
    rotate(a=[270, 0, 0]) 
    union() {
      // thread
      cylinder(h=16, d=3.38, $fn=26, center=true);
      // head
      translate(v=[0, 0, -9.38]) 
      cylinder(h=2.8, d=6.8, $fn=53, center=true);
      // head_clearance
      translate(v=[0, 0, -60.76]) 
      cylinder(h=100, d=6.8, $fn=53, center=true);
      translate(v=[0, 0, 6.68]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
      // inline_nut_clearance
      hull() 
      {
        translate(v=[0, 0, 6.68]) 
        // nut
        cylinder(h=2.8, d=6.8, $fn=6, center=true);
        translate(v=[0, 0, 16.68]) 
        // nut
        cylinder(h=2.8, d=6.8, $fn=6, center=true);
      }
    }
    // x_bearing_top_left
    mirror(v=[1, 0, 0]) 
    // x_bearing_top_right
    translate(v=[15.65, 0.0, 22.0]) 
    rotate(a=[90, 0, 0]) 
    rotate(a=[0, 0, 90]) 
    union() {
      // base
      // volume
      translate(v=[0.0, 0.0, 0.0]) 
      cube(size=[34.3, 30.3, 22.3], center=true);
      // bolts
      union() {
        translate(v=[-12.0, -9.0, -8.15]) 
        union() {
          // thread
          cylinder(h=10, d=4.278, $fn=33, center=true);
          // head
          translate(v=[0, 0, -6.73]) 
          cylinder(h=3.5, d=8.4, $fn=65, center=true);
          // head_clearance
          translate(v=[0, 0, -58.46]) 
          cylinder(h=100, d=8.4, $fn=65, center=true);
        }
        translate(v=[-12.0, 9.0, -8.15]) 
        union() {
          // thread
          cylinder(h=10, d=4.278, $fn=33, center=true);
          // head
          translate(v=[0, 0, -6.73]) 
          cylinder(h=3.5, d=8.4, $fn=65, center=true);
          // head_clearance
          translate(v=[0, 0, -58.46]) 
          cylinder(h=100, d=8.4, $fn=65, center=true);
        }
        translate(v=[12.0, -9.0, -8.15]) 
        union() {
          // thread
          cylinder(h=10, d=4.278, $fn=33, center=true);
          // head
          translate(v=[0, 0, -6.73]) 
          cylinder(h=3.5, d=8.4, $fn=65, center=true);
          // head_clearance
          translate(v=[0, 0, -58.46]) 
          cylinder(h=100, d=8.4, $fn=65, center=true);
        }
        translate(v=[12.0, 9.0, -8.15]) 
        union() {
          // thread
          cylinder(h=10, d=4.278, $fn=33, center=true);
          // head
          translate(v=[0, 0, -6.73]) 
          cylinder(h=3.5, d=8.4, $fn=65, center=true);
          // head_clearance
          translate(v=[0, 0, -58.46]) 
          cylinder(h=100, d=8.4, $fn=65, center=true);
        }
      }
      // rod_clearance
      rotate(a=[90, 0, 0]) 
      hull() 
      {
        cylinder(h=234.3, d=10, $fn=78, center=true);
        translate(v=[0, 10, 0]) 
        cylinder(h=234.3, d=10, $fn=78, center=true);
      }
    }
    // x_bearing_bottom
    translate(v=[0, 0.0, -22.0]) 
    rotate(a=[90, 0, 0]) 
    rotate(a=[0, 0, 90]) 
    union() {
      // base
      // volume
      translate(v=[0.0, 0.0, 0.0]) 
      cube(size=[34.3, 30.3, 22.3], center=true);
      // bolts
      union() {
        translate(v=[-12.0, -9.0, -8.15]) 
        union() {
          // thread
          cylinder(h=10, d=4.278, $fn=33, center=true);
          // head
          translate(v=[0, 0, -6.73]) 
          cylinder(h=3.5, d=8.4, $fn=65, center=true);
          // head_clearance
          translate(v=[0, 0, -58.46]) 
          cylinder(h=100, d=8.4, $fn=65, center=true);
        }
        translate(v=[-12.0, 9.0, -8.15]) 
        union() {
          // thread
          cylinder(h=10, d=4.278, $fn=33, center=true);
          // head
          translate(v=[0, 0, -6.73]) 
          cylinder(h=3.5, d=8.4, $fn=65, center=true);
          // head_clearance
          translate(v=[0, 0, -58.46]) 
          cylinder(h=100, d=8.4, $fn=65, center=true);
        }
        translate(v=[12.0, -9.0, -8.15]) 
        union() {
          // thread
          cylinder(h=10, d=4.278, $fn=33, center=true);
          // head
          translate(v=[0, 0, -6.73]) 
          cylinder(h=3.5, d=8.4, $fn=65, center=true);
          // head_clearance
          translate(v=[0, 0, -58.46]) 
          cylinder(h=100, d=8.4, $fn=65, center=true);
        }
        translate(v=[12.0, 9.0, -8.15]) 
        union() {
          // thread
          cylinder(h=10, d=4.278, $fn=33, center=true);
          // head
          translate(v=[0, 0, -6.73]) 
          cylinder(h=3.5, d=8.4, $fn=65, center=true);
          // head_clearance
          translate(v=[0, 0, -58.46]) 
          cylinder(h=100, d=8.4, $fn=65, center=true);
        }
      }
      // rod_clearance
      rotate(a=[90, 0, 0]) 
      hull() 
      {
        cylinder(h=234.3, d=10, $fn=78, center=true);
        translate(v=[0, 0, 0]) 
        cylinder(h=234.3, d=10, $fn=78, center=true);
      }
    }
    // left_pulley
    translate(v=[-23.35, 0.0, -6.35]) 
    union() {
      // body
      cylinder(h=10.7, d=15.4, $fn=120, center=true);
      // clearance
      rotate(a=[0, 0, 90]) 
      translate(v=[0.0, 10.0, 0.0]) 
      cube(size=[15.4, 20, 10.7], center=true);
    }
    // right_pulley
    mirror(v=[1, 0, 0]) 
    // left_pulley
    translate(v=[-23.35, 0.0, -6.35]) 
    union() {
      // body
      cylinder(h=10.7, d=15.4, $fn=120, center=true);
      // clearance
      rotate(a=[0, 0, 90]) 
      translate(v=[0.0, 10.0, 0.0]) 
      cube(size=[15.4, 20, 10.7], center=true);
    }
    // left_pulley_shaft
    translate(v=[-23.35, 0.0, -8.03]) 
    rotate(a=[180, 0, 0]) 
    union() {
      // thread
      cylinder(h=20, d=3.38, $fn=26, center=true);
      // head
      translate(v=[0, 0, -11.38]) 
      cylinder(h=2.8, d=6.8, $fn=53, center=true);
      // head_clearance
      translate(v=[0, 0, -62.76]) 
      cylinder(h=100, d=6.8, $fn=53, center=true);
      rotate(a=[0, 0, 90]) 
      translate(v=[0, 0, 7.7]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
    }
    // right_pulley_shaft
    mirror(v=[1, 0, 0]) 
    // left_pulley_shaft
    translate(v=[-23.35, 0.0, -8.03]) 
    rotate(a=[180, 0, 0]) 
    union() {
      // thread
      cylinder(h=20, d=3.38, $fn=26, center=true);
      // head
      translate(v=[0, 0, -11.38]) 
      cylinder(h=2.8, d=6.8, $fn=53, center=true);
      // head_clearance
      translate(v=[0, 0, -62.76]) 
      cylinder(h=100, d=6.8, $fn=53, center=true);
      rotate(a=[0, 0, 90]) 
      translate(v=[0, 0, 7.7]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
    }
  }
  difference() {
    union() {
      // clamp
      difference() {
        // volume
        translate(v=[0.0, 52.87, 0.0]) 
        cube(size=[40.0, 12.4, 11.0], center=true);
        // bottom_right_chamfer
        translate(v=[18.0, 52.87, -3.5]) 
        rotate(a=[0, 90, 0]) 
        rotate(a=[90, 0, 0]) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          cube(size=[2.04, 2.04, 12.44], center=true);
          // fillet
          cylinder(h=14.44, d=4, $fn=31, center=true);
        }
        // bottom_left_chamfer
        translate(v=[-18.0, 52.87, -3.5]) 
        rotate(a=[0, 180, 0]) 
        rotate(a=[90, 0, 0]) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          cube(size=[2.04, 2.04, 12.44], center=true);
          // fillet
          cylinder(h=14.44, d=4, $fn=31, center=true);
        }
        // top_left_chamfer
        translate(v=[-18.0, 52.87, 3.5]) 
        rotate(a=[0, 270, 0]) 
        rotate(a=[90, 0, 0]) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          cube(size=[2.04, 2.04, 12.44], center=true);
          // fillet
          cylinder(h=14.44, d=4, $fn=31, center=true);
        }
      }
      // fan_holder
      hull() 
      {
        translate(v=[13.0, 48.67, -17.0]) 
        cylinder(h=34, d=4, $fn=31, center=true);
        translate(v=[-13.0, 48.67, -17.0]) 
        cylinder(h=34, d=4, $fn=31, center=true);
        // volume
        translate(v=[0.0, 58.57, -17.5]) 
        cube(size=[40.0, 1.0, 35.0], center=true);
      }
      // sensor_holder_up
      difference() {
        // volume
        translate(v=[28.98, 50.07, 1.0]) 
        cube(size=[18.0, 18.0, 9.0], center=true);
        // front_right_chamfer
        translate(v=[31.98, 53.07, 1.0]) 
        difference() {
          // box
          translate(v=[3.0, 3.0, 0.0]) 
          cube(size=[6.04, 6.04, 9.04], center=true);
          // fillet
          cylinder(h=11.04, d=12, $fn=94, center=true);
        }
        // back_right_chamfer
        translate(v=[31.98, 47.07, 1.0]) 
        rotate(a=[0, 0, 270]) 
        difference() {
          // box
          translate(v=[3.0, 3.0, 0.0]) 
          cube(size=[6.04, 6.04, 9.04], center=true);
          // fillet
          cylinder(h=11.04, d=12, $fn=94, center=true);
        }
        // back_left_chamfer
        translate(v=[25.98, 47.07, 1.0]) 
        rotate(a=[0, 0, 180]) 
        difference() {
          // box
          translate(v=[3.0, 3.0, 0.0]) 
          cube(size=[6.04, 6.04, 9.04], center=true);
          // fillet
          cylinder(h=11.04, d=12, $fn=94, center=true);
        }
      }
      // sensor_holder_down
      translate(v=[0, 0, -14.75]) 
      mirror(v=[0, 0, 1]) 
      translate(v=[0, 0, 14.75]) 
      // sensor_holder_up
      difference() {
        // volume
        translate(v=[28.98, 50.07, 1.0]) 
        cube(size=[18.0, 18.0, 9.0], center=true);
        // front_right_chamfer
        translate(v=[31.98, 53.07, 1.0]) 
        difference() {
          // box
          translate(v=[3.0, 3.0, 0.0]) 
          cube(size=[6.04, 6.04, 9.04], center=true);
          // fillet
          cylinder(h=11.04, d=12, $fn=94, center=true);
        }
        // back_right_chamfer
        translate(v=[31.98, 47.07, 1.0]) 
        rotate(a=[0, 0, 270]) 
        difference() {
          // box
          translate(v=[3.0, 3.0, 0.0]) 
          cube(size=[6.04, 6.04, 9.04], center=true);
          // fillet
          cylinder(h=11.04, d=12, $fn=94, center=true);
        }
        // back_left_chamfer
        translate(v=[25.98, 47.07, 1.0]) 
        rotate(a=[0, 0, 180]) 
        difference() {
          // box
          translate(v=[3.0, 3.0, 0.0]) 
          cube(size=[6.04, 6.04, 9.04], center=true);
          // fillet
          cylinder(h=11.04, d=12, $fn=94, center=true);
        }
      }
      // sensor_holder_down_holder
      difference() {
        // volume
        translate(v=[0.0, 52.87, -30.5]) 
        cube(size=[40.0, 12.4, 9.0], center=true);
        // back_right_chamfer
        translate(v=[18.0, 48.67, -30.5]) 
        rotate(a=[0, 0, 270]) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          cube(size=[2.04, 2.04, 9.04], center=true);
          // fillet
          cylinder(h=11.04, d=4, $fn=31, center=true);
        }
        // back_left_chamfer
        translate(v=[-18.0, 48.67, -30.5]) 
        rotate(a=[0, 0, 180]) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          cube(size=[2.04, 2.04, 9.04], center=true);
          // fillet
          cylinder(h=11.04, d=4, $fn=31, center=true);
        }
      }
    }
    // extruder
    #translate(v=[0.0, 46.995, -54.31]) 
    // extruder
    union() {
      translate(v=[0, 0, 0.99]) 
      cylinder(h=2, d1=1, d2=6, $fn=7, center=true);
      translate(v=[0, 0, 2.98]) 
      cylinder(h=2, d=8.06, $fn=6, center=true);
      translate(v=[0, 0, 4.97]) 
      cylinder(h=2, d=5, $fn=39, center=true);
      translate(v=[0, 0, 5.96]) 
      // volume
      translate(v=[0.0, 5.5, 5.25]) 
      cube(size=[16, 20.0, 10.5], center=true);
      translate(v=[0, 0, 18.0]) 
      cylinder(h=3.1, d=4, $fn=31, center=true);
      translate(v=[0, 0, 32.04]) 
      cylinder(h=25, d=22.3, $fn=175, center=true);
      translate(v=[0, 0, 48.03]) 
      cylinder(h=7, d=16, $fn=125, center=true);
      translate(v=[0, 0, 54.42]) 
      cylinder(h=5.8, d=12, $fn=94, center=true);
      translate(v=[0, 0, 60.81]) 
      cylinder(h=7, d=16, $fn=125, center=true);
    }
    // extruder_clamp_bolt_left
    mirror(v=[1, 0, 0]) 
    // extruder_clamp_bolt_right
    translate(v=[-16.0, 44.78, 1.3]) 
    rotate(a=[90, 0, 0]) 
    union() {
      // thread
      cylinder(h=12, d=3.38, $fn=26, center=true);
      // head
      translate(v=[0, 0, -7.38]) 
      cylinder(h=2.8, d=6.8, $fn=53, center=true);
      // head_clearance
      translate(v=[0, 0, -58.76]) 
      cylinder(h=100, d=6.8, $fn=53, center=true);
      rotate(a=[0, 0, 90]) 
      translate(v=[0, 0, 4.68]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
      // nut_clearance
      rotate(a=[0, 0, 90]) 
      translate(v=[10.0, 0.0, 4.68]) 
      cube(size=[20, 6.089, 2.8], center=true);
    }
    // extruder_clamp_bolt_right
    translate(v=[-16.0, 44.78, 1.3]) 
    rotate(a=[90, 0, 0]) 
    union() {
      // thread
      cylinder(h=12, d=3.38, $fn=26, center=true);
      // head
      translate(v=[0, 0, -7.38]) 
      cylinder(h=2.8, d=6.8, $fn=53, center=true);
      // head_clearance
      translate(v=[0, 0, -58.76]) 
      cylinder(h=100, d=6.8, $fn=53, center=true);
      rotate(a=[0, 0, 90]) 
      translate(v=[0, 0, 4.68]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
      // nut_clearance
      rotate(a=[0, 0, 90]) 
      translate(v=[10.0, 0.0, 4.68]) 
      cube(size=[20, 6.089, 2.8], center=true);
    }
    // fan
    translate(v=[0.0, 69.37, -14.7]) 
    rotate(a=[90, 0, 0]) 
    union() {
      // body
      difference() {
        // volume
        translate(v=[0.0, 0.0, 0.0]) 
        cube(size=[40.4, 40.4, 20.4], center=true);
        // front_right_chamfer
        translate(v=[18.2, 18.2, 0.0]) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          cube(size=[2.04, 2.04, 20.44], center=true);
          // fillet
          cylinder(h=22.44, d=4, $fn=31, center=true);
        }
        // back_right_chamfer
        translate(v=[18.2, -18.2, 0.0]) 
        rotate(a=[0, 0, 270]) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          cube(size=[2.04, 2.04, 20.44], center=true);
          // fillet
          cylinder(h=22.44, d=4, $fn=31, center=true);
        }
        // back_left_chamfer
        translate(v=[-18.2, -18.2, 0.0]) 
        rotate(a=[0, 0, 180]) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          cube(size=[2.04, 2.04, 20.44], center=true);
          // fillet
          cylinder(h=22.44, d=4, $fn=31, center=true);
        }
        // front_left_chamfer
        translate(v=[-18.2, 18.2, 0.0]) 
        rotate(a=[0, 0, 90]) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          cube(size=[2.04, 2.04, 20.44], center=true);
          // fillet
          cylinder(h=22.44, d=4, $fn=31, center=true);
        }
      }
      // bolts
      translate(v=[0, 0, 5.08]) 
      {
        rotate(a=[0, 0, 225]) 
        translate(v=[22.6274, 0, 0]) 
        union() {
          // thread
          cylinder(h=25, d=3.38, $fn=26, center=true);
          // head
          translate(v=[0, 0, -13.88]) 
          cylinder(h=2.8, d=6.8, $fn=53, center=true);
          // head_clearance
          translate(v=[0, 0, -65.26]) 
          cylinder(h=100, d=6.8, $fn=53, center=true);
          translate(v=[0, 0, 11.18]) 
          // nut
          cylinder(h=2.8, d=6.8, $fn=6, center=true);
        }
        rotate(a=[0, 0, 315]) 
        translate(v=[22.6274, 0, 0]) 
        union() {
          // thread
          cylinder(h=25, d=3.38, $fn=26, center=true);
          // head
          translate(v=[0, 0, -13.88]) 
          cylinder(h=2.8, d=6.8, $fn=53, center=true);
          // head_clearance
          translate(v=[0, 0, -65.26]) 
          cylinder(h=100, d=6.8, $fn=53, center=true);
          translate(v=[0, 0, 11.18]) 
          // nut
          cylinder(h=2.8, d=6.8, $fn=6, center=true);
        }
      }
      // bolts
      translate(v=[0, 0, 12.58]) 
      {
        rotate(a=[0, 0, 45]) 
        translate(v=[22.6274, 0, 0]) 
        union() {
          // thread
          cylinder(h=40, d=3.38, $fn=26, center=true);
          // head
          translate(v=[0, 0, -21.38]) 
          cylinder(h=2.8, d=6.8, $fn=53, center=true);
          // head_clearance
          translate(v=[0, 0, -72.76]) 
          cylinder(h=100, d=6.8, $fn=53, center=true);
        }
        rotate(a=[0, 0, 135]) 
        translate(v=[22.6274, 0, 0]) 
        union() {
          // thread
          cylinder(h=40, d=3.38, $fn=26, center=true);
          // head
          translate(v=[0, 0, -21.38]) 
          cylinder(h=2.8, d=6.8, $fn=53, center=true);
          // head_clearance
          translate(v=[0, 0, -72.76]) 
          cylinder(h=100, d=6.8, $fn=53, center=true);
        }
      }
    }
    // tunnel
    hull() 
    {
      difference() {
        // volume
        translate(v=[0.0, 59.57, -20.5]) 
        cube(size=[32.0, 1.0, 34.0], center=true);
        // top_right_chamfer
        translate(v=[8.0, 59.57, -11.5]) 
        rotate(a=[90, 0, 0]) 
        difference() {
          // box
          translate(v=[4.0, 4.0, 0.0]) 
          cube(size=[8.04, 8.04, 1.04], center=true);
          // fillet
          cylinder(h=3.04, d=16, $fn=125, center=true);
        }
        // top_left_chamfer
        translate(v=[-8.0, 59.57, -11.5]) 
        rotate(a=[0, 270, 0]) 
        rotate(a=[90, 0, 0]) 
        difference() {
          // box
          translate(v=[4.0, 4.0, 0.0]) 
          cube(size=[8.04, 8.04, 1.04], center=true);
          // fillet
          cylinder(h=3.04, d=16, $fn=125, center=true);
        }
      }
      difference() {
        // volume
        translate(v=[0.0, 46.17, -21.5]) 
        cube(size=[24.0, 1.0, 32.0], center=true);
        // top_right_chamfer
        translate(v=[4.0, 46.17, -13.5]) 
        rotate(a=[90, 0, 0]) 
        difference() {
          // box
          translate(v=[4.0, 4.0, 0.0]) 
          cube(size=[8.04, 8.04, 1.04], center=true);
          // fillet
          cylinder(h=3.04, d=16, $fn=125, center=true);
        }
        // top_left_chamfer
        translate(v=[-4.0, 46.17, -13.5]) 
        rotate(a=[0, 270, 0]) 
        rotate(a=[90, 0, 0]) 
        difference() {
          // box
          translate(v=[4.0, 4.0, 0.0]) 
          cube(size=[8.04, 8.04, 1.04], center=true);
          // fillet
          cylinder(h=3.04, d=16, $fn=125, center=true);
        }
      }
    }
    // sensor
    #translate(v=[28.98, 50.07, -20.32]) 
    // sensor
    cylinder(h=60, d=12, $fn=94, center=true);
  }
  difference() {
    union() {
      // tunnel
      difference() {
        // volume
        translate(v=[-0.189, 26.92, -19.565]) 
        cube(size=[19.2, 15.3, 39.13], center=true);
        // volume
        translate(v=[-0.189, 26.92, -19.565]) 
        cube(size=[17.4, 13.5, 39.17], center=true);
      }
      // blower
      difference() {
        hull() 
        {
          // volume
          translate(v=[0.0, 39.67, -53.31]) 
          cube(size=[30.0, 5.0, 0.02], center=true);
          // volume
          translate(v=[-0.189, 26.92, -39.14]) 
          cube(size=[19.2, 15.3, 0.02], center=true);
        }
        hull() 
        {
          // volume
          translate(v=[0.0, 40.67, -54.31]) 
          cube(size=[28.0, 3.0, 0.02], center=true);
          // volume
          translate(v=[-0.189, 26.92, -39.12]) 
          cube(size=[17.2, 13.3, 0.02], center=true);
        }
      }
      // bolt_holder_left
      difference() {
        // volume
        translate(v=[-13.789, 20.77, -17.13]) 
        cube(size=[8.04, 3.0, 15.0], center=true);
        // top_left_chamfer
        translate(v=[-9.829, 20.77, -17.61]) 
        rotate(a=[0, 270, 0]) 
        rotate(a=[90, 0, 0]) 
        difference() {
          // box
          translate(v=[4.0, 4.0, 0.0]) 
          cube(size=[8, 8, 3.04], center=true);
          // chamfer
          rotate(a=[0, 0, 45]) 
          cube(size=[11.3137, 11.3137, 5.04], center=true);
        }
        // bottom_left_chamfer
        translate(v=[-15.809, 20.77, -22.63]) 
        rotate(a=[0, 180, 0]) 
        rotate(a=[90, 0, 0]) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          cube(size=[2.04, 2.04, 3.04], center=true);
          // fillet
          cylinder(h=5.04, d=4, $fn=31, center=true);
        }
      }
      // bolt_holder_right
      translate(v=[-0.189, 0, 0]) 
      mirror(v=[1, 0, 0]) 
      translate(v=[0.189, 0, 0]) 
      // bolt_holder_left
      difference() {
        // volume
        translate(v=[-13.789, 20.77, -17.13]) 
        cube(size=[8.04, 3.0, 15.0], center=true);
        // top_left_chamfer
        translate(v=[-9.829, 20.77, -17.61]) 
        rotate(a=[0, 270, 0]) 
        rotate(a=[90, 0, 0]) 
        difference() {
          // box
          translate(v=[4.0, 4.0, 0.0]) 
          cube(size=[8, 8, 3.04], center=true);
          // chamfer
          rotate(a=[0, 0, 45]) 
          cube(size=[11.3137, 11.3137, 5.04], center=true);
        }
        // bottom_left_chamfer
        translate(v=[-15.809, 20.77, -22.63]) 
        rotate(a=[0, 180, 0]) 
        rotate(a=[90, 0, 0]) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          cube(size=[2.04, 2.04, 3.04], center=true);
          // fillet
          cylinder(h=5.04, d=4, $fn=31, center=true);
        }
      }
    }
    // bolt_left
    translate(v=[-13.789, 17.78, -19.13]) 
    rotate(a=[90, 0, 0]) 
    union() {
      // thread
      cylinder(h=10, d=3.38, $fn=26, center=true);
      // head
      translate(v=[0, 0, -6.38]) 
      cylinder(h=2.8, d=6.8, $fn=53, center=true);
      // head_clearance
      translate(v=[0, 0, -57.76]) 
      cylinder(h=100, d=6.8, $fn=53, center=true);
    }
    // bolt_right
    translate(v=[-0.189, 0, 0]) 
    mirror(v=[1, 0, 0]) 
    translate(v=[-13.6, 17.78, -19.13]) 
    rotate(a=[90, 0, 0]) 
    union() {
      // thread
      cylinder(h=10, d=3.38, $fn=26, center=true);
      // head
      translate(v=[0, 0, -6.38]) 
      cylinder(h=2.8, d=6.8, $fn=53, center=true);
      // head_clearance
      translate(v=[0, 0, -57.76]) 
      cylinder(h=100, d=6.8, $fn=53, center=true);
    }
  }
}