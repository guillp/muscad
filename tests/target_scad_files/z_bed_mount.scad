union() {
  mirror(v=[1, 0, 0]) 
  difference() {
    union() {
      // base
      translate(v=[0, 0, -6.0]) 
      linear_extrude(height=31.38, center=false, convexity=10, twist=0.0, scale=1.0) 
      hull() 
      {
        translate(v=[65.0, 0.0, 0]) 
        circle(d=30, $fn=235);
        translate(v=[84.55, 41.2, 0]) 
        circle(d=8, $fn=62);
        translate(v=[45.45, 41.2, 0]) 
        circle(d=8, $fn=62);
        translate(v=[84.55, 21.0, 0]) 
        circle(d=8, $fn=62);
        translate(v=[45.45, 21.0, 0]) 
        circle(d=8, $fn=62);
      }
      // arm
      translate(v=[0, 0, -6.0]) 
      linear_extrude(height=2, center=false, convexity=10, twist=0.0, scale=1.0) 
      translate(v=[29.4, 0.0, 0]) 
      square(size=[58.8, 26.0], center=true);
      // brass_nut_cylinder
      translate(v=[0, 0, 0.48]) 
      cylinder(h=9, d=21.8, $fn=171, center=true);
      // rings
      translate(v=[0, 0, -4.02]) 
      linear_extrude(height=7, center=false, convexity=10, twist=0.0, scale=1.0) 
      intersection() {
        translate(v=[29.4, 0.0, 0]) 
        square(size=[58.8, 26.0], center=true);
        union() {
          difference() {
            circle(d=24, $fn=188);
            circle(d=20, $fn=157);
          }
          difference() {
            circle(d=32, $fn=251);
            circle(d=28, $fn=219);
          }
          difference() {
            circle(d=40, $fn=314);
            circle(d=36, $fn=282);
          }
          difference() {
            circle(d=48, $fn=376);
            circle(d=44, $fn=345);
          }
          difference() {
            circle(d=56, $fn=439);
            circle(d=52, $fn=408);
          }
          difference() {
            circle(d=64, $fn=502);
            circle(d=60, $fn=471);
          }
          difference() {
            circle(d=72, $fn=565);
            circle(d=68, $fn=533);
          }
          difference() {
            circle(d=80, $fn=628);
            circle(d=76, $fn=596);
          }
          difference() {
            circle(d=88, $fn=690);
            circle(d=84, $fn=659);
          }
          difference() {
            circle(d=96, $fn=753);
            circle(d=92, $fn=722);
          }
          difference() {
            circle(d=104, $fn=816);
            circle(d=100, $fn=785);
          }
          difference() {
            circle(d=112, $fn=879);
            circle(d=108, $fn=847);
          }
          translate(v=[29.4, 11.5, 0]) 
          square(size=[58.8, 3], center=true);
          translate(v=[29.4, -11.5, 0]) 
          square(size=[58.8, 3], center=true);
          translate(v=[29.4, 0.0, 0]) 
          square(size=[58.8, 3], center=true);
        }
      }
    }
    // extrusion
    #translate(v=[100, 37.1, 10.1]) 
    rotate(a=[0, 270, 0]) 
    // profile
    linear_extrude(height=100, center=false, convexity=10, twist=0.0, scale=1.0) 
    hull() 
    {
      translate(v=[-8.1, -8.1, 0]) 
      circle(d=4, $fn=31);
      translate(v=[-8.1, 8.1, 0]) 
      circle(d=4, $fn=31);
      translate(v=[8.1, -8.1, 0]) 
      circle(d=4, $fn=31);
      translate(v=[8.1, 8.1, 0]) 
      circle(d=4, $fn=31);
    }
    // z_rod
    #translate(v=[65.0, 0.0, 10.1]) 
    // rod
    cylinder(h=50, d=12.4, $fn=97, center=true);
    // bearing
    #translate(v=[65.0, 0.0, 10.1]) 
    difference() {
      union() {
        // outer
        cylinder(h=30.6, d=21.2, $fn=166, center=true);
        // rod_clearance
        cylinder(h=70.6, d=14.4, $fn=113, center=true);
      }
      // inner
      cylinder(h=61.2, d=12.4, $fn=97, center=true);
    }
    union() {
      hull() 
      {
        translate(v=[47.4, 28.38, 10.1]) 
        rotate(a=[270, 0, 0]) 
        // thread
        cylinder(h=10, d=6.374, $fn=50, center=true);
        translate(v=[37.4, 28.38, 10.1]) 
        rotate(a=[270, 0, 0]) 
        // thread
        cylinder(h=10, d=6.374, $fn=50, center=true);
      }
      hull() 
      {
        translate(v=[47.4, 28.38, 10.1]) 
        rotate(a=[270, 0, 0]) 
        translate(v=[0, 0, -8.18]) 
        cylinder(h=6.4, d=11.9, $fn=93, center=true);
        translate(v=[37.4, 28.38, 10.1]) 
        rotate(a=[270, 0, 0]) 
        translate(v=[0, 0, -8.18]) 
        cylinder(h=6.4, d=11.9, $fn=93, center=true);
      }
      hull() 
      {
        translate(v=[47.4, 28.38, 10.1]) 
        rotate(a=[270, 0, 0]) 
        translate(v=[0, 0, -61.36]) 
        cylinder(h=100, d=11.9, $fn=93, center=true);
        translate(v=[37.4, 28.38, 10.1]) 
        rotate(a=[270, 0, 0]) 
        translate(v=[0, 0, -61.36]) 
        cylinder(h=100, d=11.9, $fn=93, center=true);
      }
    }
    union() {
      hull() 
      {
        translate(v=[82.6, 28.38, 10.1]) 
        rotate(a=[270, 0, 0]) 
        // thread
        cylinder(h=10, d=6.374, $fn=50, center=true);
        translate(v=[92.6, 28.38, 10.1]) 
        rotate(a=[270, 0, 0]) 
        // thread
        cylinder(h=10, d=6.374, $fn=50, center=true);
      }
      hull() 
      {
        translate(v=[82.6, 28.38, 10.1]) 
        rotate(a=[270, 0, 0]) 
        translate(v=[0, 0, -8.18]) 
        cylinder(h=6.4, d=11.9, $fn=93, center=true);
        translate(v=[92.6, 28.38, 10.1]) 
        rotate(a=[270, 0, 0]) 
        translate(v=[0, 0, -8.18]) 
        cylinder(h=6.4, d=11.9, $fn=93, center=true);
      }
      hull() 
      {
        translate(v=[82.6, 28.38, 10.1]) 
        rotate(a=[270, 0, 0]) 
        translate(v=[0, 0, -61.36]) 
        cylinder(h=100, d=11.9, $fn=93, center=true);
        translate(v=[92.6, 28.38, 10.1]) 
        rotate(a=[270, 0, 0]) 
        translate(v=[0, 0, -61.36]) 
        cylinder(h=100, d=11.9, $fn=93, center=true);
      }
    }
    // top_bolt
    translate(v=[65.0, 37.1, 1.19]) 
    union() {
      // thread
      cylinder(h=10, d=6.374, $fn=50, center=true);
      // head
      translate(v=[0, 0, -8.18]) 
      cylinder(h=6.4, d=11.9, $fn=93, center=true);
      // head_clearance
      translate(v=[0, 0, -61.36]) 
      cylinder(h=100, d=11.9, $fn=93, center=true);
    }
    // insert
    translate(v=[65.0, -20.0, 9.69]) 
    cube(size=[14.4, 40, 33.38], center=true);
    // t8
    #translate(v=[0.0, 0.0, -7.0]) 
    union() {
      // rod
      cylinder(h=100, d=8.4, $fn=65, center=true);
      // brass_nut
      translate(v=[0, 0, -0.95]) 
      union() {
        // large_cylinder
        cylinder(h=3.9, d=22.4, $fn=175, center=true);
        // long_cylinder
        translate(v=[0, 0, -1.5]) 
        cylinder(h=15.4, d=10.6, $fn=83, center=true);
        // bolts
        translate(v=[0, 0, 8.83]) 
        {
          translate(v=[8, 0, 0]) 
          union() {
            // thread
            cylinder(h=16, d=3.38, $fn=26, center=true);
            // head
            translate(v=[0, 0, -9.38]) 
            cylinder(h=2.8, d=6.8, $fn=53, center=true);
            // head_clearance
            translate(v=[0, 0, -60.76]) 
            cylinder(h=100, d=6.8, $fn=53, center=true);
            translate(v=[0, 0, 3.7]) 
            // nut
            cylinder(h=2.8, d=6.8, $fn=6, center=true);
            // inline_nut_clearance
            hull() 
            {
              translate(v=[0, 0, 3.7]) 
              // nut
              cylinder(h=2.8, d=6.8, $fn=6, center=true);
              translate(v=[0, 0, 8.7]) 
              // nut
              cylinder(h=2.8, d=6.8, $fn=6, center=true);
            }
          }
          rotate(a=[0, 0, 90]) 
          translate(v=[8, 0, 0]) 
          union() {
            // thread
            cylinder(h=16, d=3.38, $fn=26, center=true);
            // head
            translate(v=[0, 0, -9.38]) 
            cylinder(h=2.8, d=6.8, $fn=53, center=true);
            // head_clearance
            translate(v=[0, 0, -60.76]) 
            cylinder(h=100, d=6.8, $fn=53, center=true);
            translate(v=[0, 0, 3.7]) 
            // nut
            cylinder(h=2.8, d=6.8, $fn=6, center=true);
            // inline_nut_clearance
            hull() 
            {
              translate(v=[0, 0, 3.7]) 
              // nut
              cylinder(h=2.8, d=6.8, $fn=6, center=true);
              translate(v=[0, 0, 8.7]) 
              // nut
              cylinder(h=2.8, d=6.8, $fn=6, center=true);
            }
          }
          rotate(a=[0, 0, 180]) 
          translate(v=[8, 0, 0]) 
          union() {
            // thread
            cylinder(h=16, d=3.38, $fn=26, center=true);
            // head
            translate(v=[0, 0, -9.38]) 
            cylinder(h=2.8, d=6.8, $fn=53, center=true);
            // head_clearance
            translate(v=[0, 0, -60.76]) 
            cylinder(h=100, d=6.8, $fn=53, center=true);
            translate(v=[0, 0, 3.7]) 
            // nut
            cylinder(h=2.8, d=6.8, $fn=6, center=true);
            // inline_nut_clearance
            hull() 
            {
              translate(v=[0, 0, 3.7]) 
              // nut
              cylinder(h=2.8, d=6.8, $fn=6, center=true);
              translate(v=[0, 0, 8.7]) 
              // nut
              cylinder(h=2.8, d=6.8, $fn=6, center=true);
            }
          }
          rotate(a=[0, 0, 270]) 
          translate(v=[8, 0, 0]) 
          union() {
            // thread
            cylinder(h=16, d=3.38, $fn=26, center=true);
            // head
            translate(v=[0, 0, -9.38]) 
            cylinder(h=2.8, d=6.8, $fn=53, center=true);
            // head_clearance
            translate(v=[0, 0, -60.76]) 
            cylinder(h=100, d=6.8, $fn=53, center=true);
            translate(v=[0, 0, 3.7]) 
            // nut
            cylinder(h=2.8, d=6.8, $fn=6, center=true);
            // inline_nut_clearance
            hull() 
            {
              translate(v=[0, 0, 3.7]) 
              // nut
              cylinder(h=2.8, d=6.8, $fn=6, center=true);
              translate(v=[0, 0, 8.7]) 
              // nut
              cylinder(h=2.8, d=6.8, $fn=6, center=true);
            }
          }
        }
      }
    }
    // tilted_clearance
    union() {
      translate(v=[50.0, 16.9, 10.1]) 
      rotate(a=[0, 270, 0]) 
      difference() {
        // box
        translate(v=[10.1, 10.1, 0.0]) 
        cube(size=[20.2, 20.2, 100], center=true);
        // chamfer
        rotate(a=[0, 0, 45]) 
        cube(size=[28.5671, 28.5671, 102], center=true);
      }
      // volume
      translate(v=[50.0, 37.1, 22.79]) 
      cube(size=[100, 20.2, 7.18], center=true);
    }
  }
  difference() {
    union() {
      // base
      translate(v=[0, 0, -6.0]) 
      linear_extrude(height=31.38, center=false, convexity=10, twist=0.0, scale=1.0) 
      hull() 
      {
        translate(v=[65.0, 0.0, 0]) 
        circle(d=30, $fn=235);
        translate(v=[84.55, 41.2, 0]) 
        circle(d=8, $fn=62);
        translate(v=[45.45, 41.2, 0]) 
        circle(d=8, $fn=62);
        translate(v=[84.55, 21.0, 0]) 
        circle(d=8, $fn=62);
        translate(v=[45.45, 21.0, 0]) 
        circle(d=8, $fn=62);
      }
      // arm
      translate(v=[0, 0, -6.0]) 
      linear_extrude(height=2, center=false, convexity=10, twist=0.0, scale=1.0) 
      translate(v=[29.4, 0.0, 0]) 
      square(size=[58.8, 26.0], center=true);
      // brass_nut_cylinder
      translate(v=[0, 0, 0.48]) 
      cylinder(h=9, d=21.8, $fn=171, center=true);
      // rings
      translate(v=[0, 0, -4.02]) 
      linear_extrude(height=7, center=false, convexity=10, twist=0.0, scale=1.0) 
      intersection() {
        translate(v=[29.4, 0.0, 0]) 
        square(size=[58.8, 26.0], center=true);
        union() {
          difference() {
            circle(d=24, $fn=188);
            circle(d=20, $fn=157);
          }
          difference() {
            circle(d=32, $fn=251);
            circle(d=28, $fn=219);
          }
          difference() {
            circle(d=40, $fn=314);
            circle(d=36, $fn=282);
          }
          difference() {
            circle(d=48, $fn=376);
            circle(d=44, $fn=345);
          }
          difference() {
            circle(d=56, $fn=439);
            circle(d=52, $fn=408);
          }
          difference() {
            circle(d=64, $fn=502);
            circle(d=60, $fn=471);
          }
          difference() {
            circle(d=72, $fn=565);
            circle(d=68, $fn=533);
          }
          difference() {
            circle(d=80, $fn=628);
            circle(d=76, $fn=596);
          }
          difference() {
            circle(d=88, $fn=690);
            circle(d=84, $fn=659);
          }
          difference() {
            circle(d=96, $fn=753);
            circle(d=92, $fn=722);
          }
          difference() {
            circle(d=104, $fn=816);
            circle(d=100, $fn=785);
          }
          difference() {
            circle(d=112, $fn=879);
            circle(d=108, $fn=847);
          }
          translate(v=[29.4, 11.5, 0]) 
          square(size=[58.8, 3], center=true);
          translate(v=[29.4, -11.5, 0]) 
          square(size=[58.8, 3], center=true);
          translate(v=[29.4, 0.0, 0]) 
          square(size=[58.8, 3], center=true);
        }
      }
    }
    // extrusion
    #translate(v=[100, 37.1, 10.1]) 
    rotate(a=[0, 270, 0]) 
    // profile
    linear_extrude(height=100, center=false, convexity=10, twist=0.0, scale=1.0) 
    hull() 
    {
      translate(v=[-8.1, -8.1, 0]) 
      circle(d=4, $fn=31);
      translate(v=[-8.1, 8.1, 0]) 
      circle(d=4, $fn=31);
      translate(v=[8.1, -8.1, 0]) 
      circle(d=4, $fn=31);
      translate(v=[8.1, 8.1, 0]) 
      circle(d=4, $fn=31);
    }
    // z_rod
    #translate(v=[65.0, 0.0, 10.1]) 
    // rod
    cylinder(h=50, d=12.4, $fn=97, center=true);
    // bearing
    #translate(v=[65.0, 0.0, 10.1]) 
    difference() {
      union() {
        // outer
        cylinder(h=30.6, d=21.2, $fn=166, center=true);
        // rod_clearance
        cylinder(h=70.6, d=14.4, $fn=113, center=true);
      }
      // inner
      cylinder(h=61.2, d=12.4, $fn=97, center=true);
    }
    union() {
      hull() 
      {
        translate(v=[47.4, 28.38, 10.1]) 
        rotate(a=[270, 0, 0]) 
        // thread
        cylinder(h=10, d=6.374, $fn=50, center=true);
        translate(v=[37.4, 28.38, 10.1]) 
        rotate(a=[270, 0, 0]) 
        // thread
        cylinder(h=10, d=6.374, $fn=50, center=true);
      }
      hull() 
      {
        translate(v=[47.4, 28.38, 10.1]) 
        rotate(a=[270, 0, 0]) 
        translate(v=[0, 0, -8.18]) 
        cylinder(h=6.4, d=11.9, $fn=93, center=true);
        translate(v=[37.4, 28.38, 10.1]) 
        rotate(a=[270, 0, 0]) 
        translate(v=[0, 0, -8.18]) 
        cylinder(h=6.4, d=11.9, $fn=93, center=true);
      }
      hull() 
      {
        translate(v=[47.4, 28.38, 10.1]) 
        rotate(a=[270, 0, 0]) 
        translate(v=[0, 0, -61.36]) 
        cylinder(h=100, d=11.9, $fn=93, center=true);
        translate(v=[37.4, 28.38, 10.1]) 
        rotate(a=[270, 0, 0]) 
        translate(v=[0, 0, -61.36]) 
        cylinder(h=100, d=11.9, $fn=93, center=true);
      }
    }
    union() {
      hull() 
      {
        translate(v=[82.6, 28.38, 10.1]) 
        rotate(a=[270, 0, 0]) 
        // thread
        cylinder(h=10, d=6.374, $fn=50, center=true);
        translate(v=[92.6, 28.38, 10.1]) 
        rotate(a=[270, 0, 0]) 
        // thread
        cylinder(h=10, d=6.374, $fn=50, center=true);
      }
      hull() 
      {
        translate(v=[82.6, 28.38, 10.1]) 
        rotate(a=[270, 0, 0]) 
        translate(v=[0, 0, -8.18]) 
        cylinder(h=6.4, d=11.9, $fn=93, center=true);
        translate(v=[92.6, 28.38, 10.1]) 
        rotate(a=[270, 0, 0]) 
        translate(v=[0, 0, -8.18]) 
        cylinder(h=6.4, d=11.9, $fn=93, center=true);
      }
      hull() 
      {
        translate(v=[82.6, 28.38, 10.1]) 
        rotate(a=[270, 0, 0]) 
        translate(v=[0, 0, -61.36]) 
        cylinder(h=100, d=11.9, $fn=93, center=true);
        translate(v=[92.6, 28.38, 10.1]) 
        rotate(a=[270, 0, 0]) 
        translate(v=[0, 0, -61.36]) 
        cylinder(h=100, d=11.9, $fn=93, center=true);
      }
    }
    // top_bolt
    translate(v=[65.0, 37.1, 1.19]) 
    union() {
      // thread
      cylinder(h=10, d=6.374, $fn=50, center=true);
      // head
      translate(v=[0, 0, -8.18]) 
      cylinder(h=6.4, d=11.9, $fn=93, center=true);
      // head_clearance
      translate(v=[0, 0, -61.36]) 
      cylinder(h=100, d=11.9, $fn=93, center=true);
    }
    // insert
    translate(v=[65.0, -20.0, 9.69]) 
    cube(size=[14.4, 40, 33.38], center=true);
    // t8
    #translate(v=[0.0, 0.0, -7.0]) 
    union() {
      // rod
      cylinder(h=100, d=8.4, $fn=65, center=true);
      // brass_nut
      translate(v=[0, 0, -0.95]) 
      union() {
        // large_cylinder
        cylinder(h=3.9, d=22.4, $fn=175, center=true);
        // long_cylinder
        translate(v=[0, 0, -1.5]) 
        cylinder(h=15.4, d=10.6, $fn=83, center=true);
        // bolts
        translate(v=[0, 0, 8.83]) 
        {
          translate(v=[8, 0, 0]) 
          union() {
            // thread
            cylinder(h=16, d=3.38, $fn=26, center=true);
            // head
            translate(v=[0, 0, -9.38]) 
            cylinder(h=2.8, d=6.8, $fn=53, center=true);
            // head_clearance
            translate(v=[0, 0, -60.76]) 
            cylinder(h=100, d=6.8, $fn=53, center=true);
            translate(v=[0, 0, 3.7]) 
            // nut
            cylinder(h=2.8, d=6.8, $fn=6, center=true);
            // inline_nut_clearance
            hull() 
            {
              translate(v=[0, 0, 3.7]) 
              // nut
              cylinder(h=2.8, d=6.8, $fn=6, center=true);
              translate(v=[0, 0, 8.7]) 
              // nut
              cylinder(h=2.8, d=6.8, $fn=6, center=true);
            }
          }
          rotate(a=[0, 0, 90]) 
          translate(v=[8, 0, 0]) 
          union() {
            // thread
            cylinder(h=16, d=3.38, $fn=26, center=true);
            // head
            translate(v=[0, 0, -9.38]) 
            cylinder(h=2.8, d=6.8, $fn=53, center=true);
            // head_clearance
            translate(v=[0, 0, -60.76]) 
            cylinder(h=100, d=6.8, $fn=53, center=true);
            translate(v=[0, 0, 3.7]) 
            // nut
            cylinder(h=2.8, d=6.8, $fn=6, center=true);
            // inline_nut_clearance
            hull() 
            {
              translate(v=[0, 0, 3.7]) 
              // nut
              cylinder(h=2.8, d=6.8, $fn=6, center=true);
              translate(v=[0, 0, 8.7]) 
              // nut
              cylinder(h=2.8, d=6.8, $fn=6, center=true);
            }
          }
          rotate(a=[0, 0, 180]) 
          translate(v=[8, 0, 0]) 
          union() {
            // thread
            cylinder(h=16, d=3.38, $fn=26, center=true);
            // head
            translate(v=[0, 0, -9.38]) 
            cylinder(h=2.8, d=6.8, $fn=53, center=true);
            // head_clearance
            translate(v=[0, 0, -60.76]) 
            cylinder(h=100, d=6.8, $fn=53, center=true);
            translate(v=[0, 0, 3.7]) 
            // nut
            cylinder(h=2.8, d=6.8, $fn=6, center=true);
            // inline_nut_clearance
            hull() 
            {
              translate(v=[0, 0, 3.7]) 
              // nut
              cylinder(h=2.8, d=6.8, $fn=6, center=true);
              translate(v=[0, 0, 8.7]) 
              // nut
              cylinder(h=2.8, d=6.8, $fn=6, center=true);
            }
          }
          rotate(a=[0, 0, 270]) 
          translate(v=[8, 0, 0]) 
          union() {
            // thread
            cylinder(h=16, d=3.38, $fn=26, center=true);
            // head
            translate(v=[0, 0, -9.38]) 
            cylinder(h=2.8, d=6.8, $fn=53, center=true);
            // head_clearance
            translate(v=[0, 0, -60.76]) 
            cylinder(h=100, d=6.8, $fn=53, center=true);
            translate(v=[0, 0, 3.7]) 
            // nut
            cylinder(h=2.8, d=6.8, $fn=6, center=true);
            // inline_nut_clearance
            hull() 
            {
              translate(v=[0, 0, 3.7]) 
              // nut
              cylinder(h=2.8, d=6.8, $fn=6, center=true);
              translate(v=[0, 0, 8.7]) 
              // nut
              cylinder(h=2.8, d=6.8, $fn=6, center=true);
            }
          }
        }
      }
    }
    // tilted_clearance
    union() {
      translate(v=[50.0, 16.9, 10.1]) 
      rotate(a=[0, 270, 0]) 
      difference() {
        // box
        translate(v=[10.1, 10.1, 0.0]) 
        cube(size=[20.2, 20.2, 100], center=true);
        // chamfer
        rotate(a=[0, 0, 45]) 
        cube(size=[28.5671, 28.5671, 102], center=true);
      }
      // volume
      translate(v=[50.0, 37.1, 22.79]) 
      cube(size=[100, 20.2, 7.18], center=true);
    }
  }
}