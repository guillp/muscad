difference() {
  union() {
    // body
    difference() {
      // volume
      translate(v=[-14.0, 10.6, 9.1]) 
      cube(size=[20.4, 33.2, 34.2], center=true);
      // top_right_chamfer
      translate(v=[-7.8, 27.22, 22.2]) 
      rotate(a=[90, 0, 0]) 
      linear_extrude(height=33.24, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        square(size=[4.04, 4.04], center=true);
        // fillet
        circle(d=8, $fn=62);
      }
      // top_left_chamfer
      translate(v=[-20.2, 27.22, 22.2]) 
      rotate(a=[90, 270, 0]) 
      linear_extrude(height=33.24, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        square(size=[4.04, 4.04], center=true);
        // fillet
        circle(d=8, $fn=62);
      }
      // front_right_chamfer
      translate(v=[-9.8, 21.2, -8.02]) 
      linear_extrude(height=34.24, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[3.0, 3.0, 0.0]) 
        square(size=[6.04, 6.04], center=true);
        // fillet
        circle(d=12, $fn=94);
      }
      // front_left_chamfer
      translate(v=[-18.2, 21.2, -8.02]) 
      rotate(a=[0, 0, 90]) 
      linear_extrude(height=34.24, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[3.0, 3.0, 0.0]) 
        square(size=[6.04, 6.04], center=true);
        // fillet
        circle(d=12, $fn=94);
      }
      // back_left_chamfer
      translate(v=[-20.2, -2.0, -8.02]) 
      rotate(a=[0, 0, 180]) 
      linear_extrude(height=34.24, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        square(size=[4.04, 4.04], center=true);
        // fillet
        circle(d=8, $fn=62);
      }
    }
    // rod_holder
    translate(v=[0, 0, -8.0]) 
    linear_extrude(height=15, center=false, convexity=10, twist=0.0, scale=1.0) 
    hull() 
    {
      translate(v=[-20.2, -2.0, 0]) 
      circle(d=8, $fn=62);
      translate(v=[-6.2, -28.2, 0]) 
      circle(d=6, $fn=47);
      translate(v=[8.2, -28.2, 0]) 
      circle(d=6, $fn=47);
      translate(v=[9.2, -4.8, 0]) 
      circle(d=4, $fn=31);
    }
  }
  // extrusion
  #translate(v=[-25.0, 15.1, 15.1]) 
  rotate(a=[0, 90, 0]) 
  // profile
  linear_extrude(height=50, center=false, convexity=10, twist=0.0, scale=1.0) 
  hull() 
  {
    translate(v=[-13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[-13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
  }
  // rod
  #translate(v=[0.0, -22.0, 16.98]) 
  // rod
  cylinder(h=50, d=12.4, $fn=97, center=true);
  // rod_holder_bolt
  translate(v=[1.2, -10.8, -0.5]) 
  rotate(a=[0, 90, 0]) 
  union() {
    // thread
    cylinder(h=20, d=3.38, $fn=26, center=true);
    // head
    translate(v=[0, 0, -11.38]) 
    cylinder(h=2.8, d=6.8, $fn=53, center=true);
    // head_clearance
    translate(v=[0, 0, -62.76]) 
    cylinder(h=100, d=6.8, $fn=53, center=true);
    rotate(a=[0, 0, 90]) 
    translate(v=[0, 0, 8.6]) 
    // nut
    cylinder(h=2.8, d=6.8, $fn=6, center=true);
    // inline_nut_clearance
    hull() 
    {
      rotate(a=[0, 0, 90]) 
      translate(v=[0, 0, 8.6]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
      translate(v=[0, 0, 20]) 
      rotate(a=[0, 0, 90]) 
      translate(v=[0, 0, 8.6]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
    }
  }
  // rod_holder_clearance
  translate(v=[0.0, -6.8, -0.5]) 
  cube(size=[2, 20, 16.0], center=true);
  // bottom_bolt
  translate(v=[-14.0, 15.1, 1.19]) 
  union() {
    // thread
    cylinder(h=10, d=6.374, $fn=50, center=true);
    // head
    translate(v=[0, 0, -8.18]) 
    cylinder(h=6.4, d=11.9, $fn=93, center=true);
    // head_clearance
    translate(v=[0, 0, -61.36]) 
    cylinder(h=100, d=11.9, $fn=93, center=true);
  }
  // front_bolt
  translate(v=[-14.0, 1.19, 15.1]) 
  rotate(a=[270, 0, 0]) 
  union() {
    hull() 
    {
      // thread
      cylinder(h=10, d=6.374, $fn=50, center=true);
      translate(v=[-20, 0, 0]) 
      // thread
      cylinder(h=10, d=6.374, $fn=50, center=true);
    }
    hull() 
    {
      translate(v=[0, 0, -8.18]) 
      cylinder(h=6.4, d=11.9, $fn=93, center=true);
      translate(v=[-20, 0, -8.18]) 
      cylinder(h=6.4, d=11.9, $fn=93, center=true);
    }
    hull() 
    {
      translate(v=[0, 0, -61.36]) 
      cylinder(h=100, d=11.9, $fn=93, center=true);
      translate(v=[-20, 0, -61.36]) 
      cylinder(h=100, d=11.9, $fn=93, center=true);
    }
  }
}