difference() {
  union() {
    // pulleys_box
    difference() {
      difference() {
        // volume
        translate(v=[51.35, 37.6, 16.3]) 
        cube(size=[42.3, 13.4, 47.0], center=true);
        // front_right_chamfer
        translate(v=[67.5, 39.3, -7.22]) 
        linear_extrude(height=47.04, center=false, convexity=10, twist=0.0, scale=1.0) 
        difference() {
          // box
          translate(v=[2.5, 2.5, 0.0]) 
          square(size=[5.04, 5.04], center=true);
          // fillet
          circle(d=10, $fn=78);
        }
        // back_right_chamfer
        translate(v=[67.5, 35.9, -7.22]) 
        rotate(a=[0, 0, 270]) 
        linear_extrude(height=47.04, center=false, convexity=10, twist=0.0, scale=1.0) 
        difference() {
          // box
          translate(v=[2.5, 2.5, 0.0]) 
          square(size=[5.04, 5.04], center=true);
          // fillet
          circle(d=10, $fn=78);
        }
      }
      // volume
      translate(v=[51.35, 37.6, 9.81]) 
      cube(size=[15.4, 15.4, 13.98], center=true);
    }
    // base
    difference() {
      // volume
      translate(v=[37.25, 23.15, -11.18]) 
      cube(size=[70.5, 42.3, 8.0], center=true);
      // front_right_chamfer
      translate(v=[67.5, 39.3, -15.2]) 
      linear_extrude(height=8.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[2.5, 2.5, 0.0]) 
        square(size=[5.04, 5.04], center=true);
        // fillet
        circle(d=10, $fn=78);
      }
      // front_left_chamfer
      translate(v=[7.0, 39.3, -15.2]) 
      rotate(a=[0, 0, 90]) 
      linear_extrude(height=8.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[2.5, 2.5, 0.0]) 
        square(size=[5.04, 5.04], center=true);
        // fillet
        circle(d=10, $fn=78);
      }
      // back_right_chamfer
      translate(v=[67.5, 7.0, -15.2]) 
      rotate(a=[0, 0, 270]) 
      linear_extrude(height=8.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[2.5, 2.5, 0.0]) 
        square(size=[5.04, 5.04], center=true);
        // fillet
        circle(d=10, $fn=78);
      }
    }
    // walls
    difference() {
      // volume
      translate(v=[22.1, 23.15, 12.31]) 
      cube(size=[40.2, 42.3, 54.98], center=true);
      // front_right_chamfer
      translate(v=[40.2, 42.3, -15.2]) 
      linear_extrude(height=55.02, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // back_right_chamfer
      translate(v=[40.2, 4.0, -15.2]) 
      rotate(a=[0, 0, 270]) 
      linear_extrude(height=55.02, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // back_left_chamfer
      translate(v=[4.0, 4.0, -15.2]) 
      rotate(a=[0, 0, 180]) 
      linear_extrude(height=55.02, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // front_left_chamfer
      translate(v=[4.0, 42.3, -15.2]) 
      rotate(a=[0, 0, 90]) 
      linear_extrude(height=55.02, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
    }
    // clamp
    difference() {
      // volume
      translate(v=[15.1, 37.24, 2.335]) 
      cube(size=[26.2, 14.12, 35.03], center=true);
      // front_left_chamfer
      translate(v=[4.0, 42.3, -15.2]) 
      rotate(a=[0, 0, 90]) 
      linear_extrude(height=35.07, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
    }
  }
  // x_inner_belt_clearance
  // volume
  translate(v=[56.35, 37.8, -2.08]) 
  cube(size=[5.0, 15.0, 10.0], center=true);
  // x_outer_belt_clearance
  // volume
  translate(v=[46.35, 37.8, -2.08]) 
  cube(size=[5.0, 15.0, 10.0], center=true);
  // inner_shaft
  translate(v=[51.35, 37.6, 2.32]) 
  rotate(a=[0, 180, 0]) 
  union() {
    // thread
    cylinder(h=35, d=3.38, $fn=26, center=true);
    // head
    translate(v=[0, 0, -18.88]) 
    cylinder(h=2.8, d=6.8, $fn=53, center=true);
    // head_clearance
    translate(v=[0, 0, -70.26]) 
    cylinder(h=100, d=6.8, $fn=53, center=true);
    rotate(a=[0, 0, 90]) 
    translate(v=[0, 0, 16.18]) 
    // nut
    cylinder(h=2.8, d=6.8, $fn=6, center=true);
  }
  // z_extrusion
  #translate(v=[15.1, 15.1, -30.0]) 
  // profile
  linear_extrude(height=100, center=false, convexity=10, twist=0.0, scale=1.0) 
  hull() 
  {
    translate(v=[-13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[-13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
  }
  // x_extrusion
  #translate(v=[80.2, 15.1, 54.9]) 
  rotate(a=[270, 0, 90]) 
  // profile
  linear_extrude(height=50, center=false, convexity=10, twist=0.0, scale=1.0) 
  hull() 
  {
    translate(v=[-13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[-13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
  }
  // y_extrusion
  #translate(v=[15.1, 80.2, 54.9]) 
  rotate(a=[270, 0, 180]) 
  // profile
  linear_extrude(height=50, center=false, convexity=10, twist=0.0, scale=1.0) 
  hull() 
  {
    translate(v=[-13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[-13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
  }
  // y_rod
  #translate(v=[15.1, 55.18, 4.8]) 
  rotate(a=[270, 0, 180]) 
  // rod
  cylinder(h=50, d=12.4, $fn=97, center=true);
  // stepper
  #translate(v=[51.35, 23.15, -57.2]) 
  union() {
    // body
    difference() {
      // volume
      translate(v=[0.0, 0.0, 21.0]) 
      cube(size=[42.3, 42.3, 42], center=true);
      // front_right_chamfer
      translate(v=[17.17, 17.17, 21.0]) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        cube(size=[4, 4, 42.04], center=true);
        // chamfer
        rotate(a=[0, 0, 45]) 
        cube(size=[5.6569, 5.6569, 44.04], center=true);
      }
      // back_right_chamfer
      translate(v=[17.17, -17.17, 21.0]) 
      rotate(a=[0, 0, 270]) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        cube(size=[4, 4, 42.04], center=true);
        // chamfer
        rotate(a=[0, 0, 45]) 
        cube(size=[5.6569, 5.6569, 44.04], center=true);
      }
      // back_left_chamfer
      translate(v=[-17.17, -17.17, 21.0]) 
      rotate(a=[0, 0, 180]) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        cube(size=[4, 4, 42.04], center=true);
        // chamfer
        rotate(a=[0, 0, 45]) 
        cube(size=[5.6569, 5.6569, 44.04], center=true);
      }
      // front_left_chamfer
      translate(v=[-17.17, 17.17, 21.0]) 
      rotate(a=[0, 0, 90]) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        cube(size=[4, 4, 42.04], center=true);
        // chamfer
        rotate(a=[0, 0, 45]) 
        cube(size=[5.6569, 5.6569, 44.04], center=true);
      }
    }
    // bolts
    translate(v=[0, 0, 43.0]) 
    {
      rotate(a=[0, 0, 45]) 
      translate(v=[21.9486, 0, 0]) 
      rotate(a=[0, 180, 0]) 
      union() {
        // thread
        cylinder(h=8, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -5.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -56.76]) 
        cylinder(h=100, d=6.8, $fn=53, center=true);
      }
      rotate(a=[0, 0, 135]) 
      translate(v=[21.9486, 0, 0]) 
      rotate(a=[0, 180, 0]) 
      union() {
        // thread
        cylinder(h=8, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -5.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -56.76]) 
        cylinder(h=100, d=6.8, $fn=53, center=true);
      }
      rotate(a=[0, 0, 225]) 
      translate(v=[21.9486, 0, 0]) 
      rotate(a=[0, 180, 0]) 
      union() {
        // thread
        cylinder(h=8, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -5.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -56.76]) 
        cylinder(h=100, d=6.8, $fn=53, center=true);
      }
      rotate(a=[0, 0, 315]) 
      translate(v=[21.9486, 0, 0]) 
      rotate(a=[0, 180, 0]) 
      union() {
        // thread
        cylinder(h=8, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -5.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -56.76]) 
        cylinder(h=100, d=6.8, $fn=53, center=true);
      }
    }
    // central_bulge
    translate(v=[0.0, 0.0, 43.0]) 
    cylinder(h=4, d=22.4, $fn=175, center=true);
    // shaft
    translate(v=[0.0, 0.0, 55.48]) 
    cylinder(h=27, d=5.4, $fn=42, center=true);
  }
  // shaft_bearing
  translate(v=[51.35, 23.15, -9.86]) 
  difference() {
    // outer
    cylinder(h=5.4, d=14.4, $fn=113, center=true);
    // inner
    cylinder(h=5.44, d=5.4, $fn=42, center=true);
  }
  // y_upper_bolt
  translate(v=[15.1, 30.2, 26.8]) 
  rotate(a=[270, 0, 180]) 
  union() {
    // thread
    cylinder(h=8, d=6.374, $fn=50, center=true);
    // head
    translate(v=[0, 0, -7.18]) 
    cylinder(h=6.4, d=11.9, $fn=93, center=true);
    // head_clearance
    translate(v=[0, 0, -60.36]) 
    cylinder(h=100, d=11.9, $fn=93, center=true);
  }
  // clamp_clearance
  union() {
    // volume
    translate(v=[15.1, 37.25, 12.325]) 
    cube(size=[1.0, 14.14, 15.05], center=true);
    // volume
    translate(v=[8.54, 37.25, 19.35]) 
    cube(size=[13.12, 14.14, 1.0], center=true);
  }
  // clamp_bolt
  #translate(v=[15.6, 37.3, 14.4]) 
  rotate(a=[270, 0, 270]) 
  union() {
    // thread
    cylinder(h=16, d=3.38, $fn=26, center=true);
    // head
    translate(v=[0, 0, -9.38]) 
    cylinder(h=2.8, d=6.8, $fn=53, center=true);
    // head_clearance
    translate(v=[0, 0, -60.76]) 
    cylinder(h=100, d=6.8, $fn=53, center=true);
    translate(v=[0, 0, 5.7]) 
    // nut
    cylinder(h=2.8, d=6.8, $fn=6, center=true);
    // inline_nut_clearance
    hull() 
    {
      translate(v=[0, 0, 5.7]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
      translate(v=[0, 0, 15.7]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
    }
    // nut_clearance
    translate(v=[5.0, 0.0, 5.7]) 
    cube(size=[10, 6.089, 2.8], center=true);
  }
  // x_lower_bolt
  translate(v=[30.2, 15.1, 1.77]) 
  rotate(a=[270, 0, 90]) 
  union() {
    // thread
    cylinder(h=8, d=6.374, $fn=50, center=true);
    // head
    translate(v=[0, 0, -7.18]) 
    cylinder(h=6.4, d=11.9, $fn=93, center=true);
    // head_clearance
    translate(v=[0, 0, -60.36]) 
    cylinder(h=100, d=11.9, $fn=93, center=true);
  }
  // x_upper_bolt
  translate(v=[30.2, 15.1, 26.8]) 
  rotate(a=[270, 0, 90]) 
  union() {
    // thread
    cylinder(h=8, d=6.374, $fn=50, center=true);
    // head
    translate(v=[0, 0, -7.18]) 
    cylinder(h=6.4, d=11.9, $fn=93, center=true);
    // head_clearance
    translate(v=[0, 0, -60.36]) 
    cylinder(h=100, d=11.9, $fn=93, center=true);
  }
  // inner_y_pulley
  translate(v=[51.35, 37.6, 11.15]) 
  // body
  cylinder(h=10.7, d=13.4, $fn=105, center=true);
  // endstop
  translate(v=[23.68, 41.07, -18.2]) 
  union() {
    // enstop
    cube(size=[13, 6.5, 6], center=true);
    // cables
    translate(v=[0.0, -8.25, 0.0]) 
    cube(size=[13, 10, 6], center=true);
    // left_bolt
    translate(v=[-3.25, -1.75, 0.0]) 
    cylinder(h=10, d=1, $fn=7, center=true);
    // right_bolt
    translate(v=[3.25, -1.75, 0.0]) 
    cylinder(h=10, d=1, $fn=7, center=true);
  }
}