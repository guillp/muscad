from muscad import E, T, Volume, Part, Fillet
from muscad.vitamins.bolts import Bolt
from muscad.vitamins.extrusions import Extrusion
from muscad.vitamins.steppers import StepperMotor


class ExtruderStepperHolder(Part):
    y_extrusion = ~Extrusion.e3030(100).bottom_to_front().debug()
    stepper = (
        ~StepperMotor.nema17(
            height=45,
            gearbox_height=24,
            shaft_diameter=9,
            bolt=Bolt.M3(10, head_clearance=4).top_to_bottom(),
        )
        .bottom_to_front()
        .align(right=y_extrusion.left - 2, top=y_extrusion.top + 6)
        .debug()
    )

    stepper_holder = (
        Volume(
            right=y_extrusion.left,
            left=stepper.left,
            front=stepper.body.back - T,
            back=stepper.back - 11,
            bottom=stepper.bottom,
            top=stepper.top,
        )
        .fillet_depth(4, bottom=True)
        .fillet_depth(4, top=True, left=True)
    )

    bolts = (
        ~Bolt.M3(16)
        .add_nut(-4, side_clearance_size=20, angle=-90)
        .bottom_to_back()
        .align(
            center_x=stepper.center_x + 15.52,
            center_y=stepper_holder.back,
            center_z=stepper.center_z + 15.52,
        )
        .x_mirror(center=stepper.center_x, keep=True)
        .z_mirror(center=stepper.center_z, keep=True)
    )

    attachment = (
        Volume(
            left=stepper_holder.right - E,
            right=y_extrusion.right - 2,
            back=stepper_holder.back,
            front=stepper.front,
            bottom=y_extrusion.top,
            top=stepper_holder.top,
        )
        .fillet_height(4, right=True)
        .fillet_height(4, front=True, left=True)
    )

    attachment_fillet = (
        Fillet(2)
        .z_linear_extrude(bottom=attachment.bottom, top=attachment.top)
        .front_to_right()
        .align(right=attachment.left, back=stepper_holder.front)
    )

    bolt_top_back = (
        ~Bolt.M6(12)
        .upside_down()
        .align(
            center_x=y_extrusion.center_x,
            center_y=stepper_holder.center_y,
            center_z=y_extrusion.top,
        )
    )
    bolt_top_front = ~bolt_top_back.y_mirror(attachment.center_y)


if __name__ == "__main__":
    ExtruderStepperHolder().render_to_file(openscad=False)
    # BowdenExtruder().render_to_file(openscad=False)
    # ExtruderBracket().render_to_file()

    # (
    #    ExtruderStepperHolder().color("green")
    #    + BowdenExtruder().color("blue")
    #    + ExtruderBracket()
    # ).render_to_file("bowden_extruder_assembled", openscad=False)
