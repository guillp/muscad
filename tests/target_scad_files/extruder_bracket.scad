difference() {
  // body
  difference() {
    // volume
    translate(v=[-59.9875, -86.2, 2.15]) 
    cube(size=[16.625, 12.0, 42.3], center=true);
    // bottom_left_chamfer
    translate(v=[-64.3, -86.2, -15.0]) 
    rotate(a=[90, 180, 0]) 
    difference() {
      // box
      translate(v=[2.0, 2.0, 0.0]) 
      cube(size=[4.04, 4.04, 12.04], center=true);
      // fillet
      cylinder(h=14.04, d=8, $fn=62, center=true);
    }
    // top_left_chamfer
    translate(v=[-64.3, -86.2, 19.3]) 
    rotate(a=[90, 270, 0]) 
    difference() {
      // box
      translate(v=[2.0, 2.0, 0.0]) 
      cube(size=[4.04, 4.04, 12.04], center=true);
      // fillet
      cylinder(h=14.04, d=8, $fn=62, center=true);
    }
  }
  // bearing_clearance
  difference() {
    translate(v=[-55.65, -95.4, 2.15]) 
    rotate(a=[270, 0, 0]) 
    cylinder(h=25.4, d=25.9, $fn=203, center=true);
    // bearing_fix
    translate(v=[-55.65, -84.95, 2.15]) 
    rotate(a=[270, 0, 0]) 
    cylinder(h=9.9, d=8.3, $fn=65, center=true);
  }
  // bracket_bolt
  translate(v=[-62.3, -74.98, 17.3]) 
  rotate(a=[270, 0, 0]) 
  union() {
    // thread
    cylinder(h=20, d=4.378, $fn=34, center=true);
    // head
    translate(v=[0, 0, -11.78]) 
    cylinder(h=3.6, d=8.5, $fn=66, center=true);
    // head_clearance
    translate(v=[0, 0, -23.56]) 
    cylinder(h=20, d=8.5, $fn=66, center=true);
    rotate(a=[0, 0, 90]) 
    translate(v=[0, 0, 8.28]) 
    // nut
    cylinder(h=3.6, d=8.5, $fn=6, center=true);
  }
  // bracket_tightening_bolt
  #translate(v=[-55.825, -86.175, -15.5]) 
  union() {
    hull() 
    {
      rotate(a=[0, 90, 0]) 
      // thread
      cylinder(h=16, d=3.38, $fn=26, center=true);
      translate(v=[0, 0, 2]) 
      rotate(a=[0, 90, 0]) 
      // thread
      cylinder(h=16, d=3.38, $fn=26, center=true);
    }
    hull() 
    {
      rotate(a=[0, 90, 0]) 
      translate(v=[0, 0, -9.38]) 
      cylinder(h=2.8, d=6.8, $fn=53, center=true);
      translate(v=[0, 0, 2]) 
      rotate(a=[0, 90, 0]) 
      translate(v=[0, 0, -9.38]) 
      cylinder(h=2.8, d=6.8, $fn=53, center=true);
    }
    hull() 
    {
      rotate(a=[0, 90, 0]) 
      translate(v=[0, 0, -60.76]) 
      cylinder(h=100, d=6.8, $fn=53, center=true);
      translate(v=[0, 0, 2]) 
      rotate(a=[0, 90, 0]) 
      translate(v=[0, 0, -60.76]) 
      cylinder(h=100, d=6.8, $fn=53, center=true);
    }
    hull() 
    {
      rotate(a=[0, 90, 0]) 
      rotate(a=[0, 0, 180]) 
      translate(v=[0, 0, 4.7]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
      translate(v=[0, 0, 2]) 
      rotate(a=[0, 90, 0]) 
      rotate(a=[0, 0, 180]) 
      translate(v=[0, 0, 4.7]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
    }
    hull() 
    {
      rotate(a=[0, 90, 0]) 
      rotate(a=[0, 0, 180]) 
      translate(v=[4.0, 0.0, 4.7]) 
      cube(size=[8, 6.089, 2.8], center=true);
      translate(v=[0, 0, 2]) 
      rotate(a=[0, 90, 0]) 
      rotate(a=[0, 0, 180]) 
      translate(v=[4.0, 0.0, 4.7]) 
      cube(size=[8, 6.089, 2.8], center=true);
    }
  }
  // bearing_bolt
  #translate(v=[-55.65, -82.62, 2.15]) 
  rotate(a=[270, 0, 0]) 
  union() {
    // thread
    cylinder(h=16, d=5.376, $fn=42, center=true);
    // head
    translate(v=[0, 0, -10.18]) 
    cylinder(h=4.4, d=9.6, $fn=75, center=true);
    // head_clearance
    translate(v=[0, 0, -62.36]) 
    cylinder(h=100, d=9.6, $fn=75, center=true);
  }
  // tightener_clearance
  difference() {
    // volume
    translate(v=[-55.165, -86.2, -13.45]) 
    cube(size=[7.02, 12.04, 11.3], center=true);
    // top_left_chamfer
    translate(v=[-56.675, -86.2, -9.8]) 
    rotate(a=[90, 270, 0]) 
    difference() {
      // box
      translate(v=[1.0, 1.0, 0.0]) 
      cube(size=[2.04, 2.04, 12.08], center=true);
      // fillet
      cylinder(h=14.08, d=4, $fn=31, center=true);
    }
  }
}