from muscad import (
    Cube,
    E,
    EE,
    Part,
    Volume,
    MirroredPart,
    Surface,
    Circle,
    Square,
)
from muscad.vitamins.bearings import RotationBearing
from muscad.vitamins.bolts import Bolt
from muscad.vitamins.extrusions import Extrusion
from muscad.vitamins.rods import Rod
from muscad.vitamins.steppers import StepperMotor


class ZStepperMount(Part):
    def __stl__(self):
        return self.top_to_bottom()

    extrusion = (
        ~Extrusion.e3030(100)
        .x_rotate(90)
        .align(left=0, center_y=0, top=0)
        .debug()
    )
    stepper = ~StepperMotor.nema17(bolt=Bolt.M3(8).top_to_bottom()).align(
        center_x=extrusion.left - 22, center_y=0, top=extrusion.top
    )

    bearing = ~RotationBearing.b605zz().align(
        center_x=stepper.center_x,
        center_y=stepper.center_y,
        bottom=stepper.top + 2,
    )

    side_bolt_front = (
        ~Bolt.M6(10)
        .slide(y=20)
        .y_rotate(90)
        .align(
            center_x=extrusion.left - 2,
            center_y=stepper.front + 15,
            center_z=extrusion.center_z,
        )
    )

    side_bolt_back = (
        ~Bolt.M6(10)
        .slide(y=-20)
        .y_rotate(90)
        .align(
            center_x=extrusion.left - 2,
            center_y=stepper.back - 15,
            center_z=extrusion.center_z,
        )
    )

    top_bolt_front = (
        ~Bolt.M6(10)
        .top_to_bottom()
        .align(
            center_x=extrusion.center_x,
            center_y=side_bolt_front.center_y - 3,
            center_z=extrusion.top + 2,
        )
    )
    top_bolt_center = (
        ~Bolt.M6(10)
        .top_to_bottom()
        .align(
            center_x=extrusion.center_x,
            center_y=stepper.center_y,
            center_z=extrusion.top + 2,
        )
    )
    top_bolt_back = (
        ~Bolt.M6(10)
        .top_to_bottom()
        .align(
            center_x=extrusion.center_x,
            center_y=side_bolt_back.center_y + 3,
            center_z=extrusion.top + 2,
        )
    )

    body = (
        Volume(
            left=extrusion.left - 6,
            right=extrusion.right - 4,
            back=top_bolt_back.back - 2,
            front=top_bolt_front.front + 2,
            bottom=extrusion.bottom + 4,
            top=extrusion.top + 7,
        )
        .fillet_width(r=5, bottom=True, front=True, back=True)
        .fillet_depth(r=5, top=True, left=True)
        .fillet_height(r=5, right=True, front=True, back=True)
    )

    stepper_box = Volume(
        left=stepper.left - 3,
        right=extrusion.left,
        back=stepper.back - 8,
        front=stepper.front + 8,
        bottom=extrusion.bottom + 4,
        top=body.top,
    ).fillet_height(r=5, left=True, front=True, back=True)

    stepper_clearance = ~Volume(
        left=stepper_box.left - 1,
        right=stepper_box.right + 1,
        back=stepper_box.back + 4,
        front=stepper_box.front - 4,
        bottom=stepper_box.bottom - 10,
        top=stepper_box.top - 8,
    ).fillet_width(r=4, top=True, front=True, back=True)

    tilted_clearance = (
        ~Cube(
            stepper_clearance.width, stepper_box.depth + EE, stepper_box.height
        )
        .align(
            right=10,
            center_y=extrusion.center_y,
            top=stepper_clearance.top - 25,
        )
        .y_rotate(35)
    )


class ZStepperMountWithZBackets(MirroredPart, x=True):
    extrusion = (
        ~Extrusion.e3030(200)
        .y_rotate(90)
        .align(center_x=0, back=0, bottom=0)
        .debug()
    )
    rod = ~Rod.d12(50).align(center_x=65, center_y=-22, bottom=-8 - E).debug()

    stepper = (
        ~StepperMotor.nema17(bolt=Bolt.M3(8).top_to_bottom())
        .align(center_x=0, center_y=-27, top=extrusion.bottom - 6)
        .debug()
    )

    stepper_attachment = (
        Volume(
            left=0,
            right=stepper.right + 5,
            back=stepper.back - 5,
            front=extrusion.back + 2,
            bottom=stepper.top,
            top=extrusion.bottom,
        )
        .fillet_height(6, back=True, right=True)
        .reverse_fillet_front(6, left=True, right=True)
    )

    stepper_reinforcement = (
        Volume(
            left=0,
            right=stepper_attachment.right,
            front=extrusion.back,
            depth=6,
            bottom=stepper_attachment.top,
            top=extrusion.top - 2,
        ).fillet_depth(6, right=True, top=True)
        + Volume(
            right=stepper_attachment.right,
            width=6,
            back=stepper_attachment.back + 6,
            front=stepper_attachment.front,
            bottom=stepper_attachment.top,
            top=extrusion.top - 8,
        ).chamfer_width(41, back=True, top=True)
    )
    stepper_reinforcement_bolt = (
        ~Bolt.M5(10)
        .bottom_to_back()
        .slide(z=30)
        .align(
            center_x=stepper_reinforcement.right - 12,
            center_y=extrusion.back,
            center_z=extrusion.center_z,
        )
    )
    bearing = ~RotationBearing.b605zz().align(
        center_x=stepper.center_x,
        center_y=stepper.center_y,
        bottom=stepper.top + 2,
    )

    side_bolt_front = (
        ~Bolt.M6(10)
        .slide(y=20)
        .y_rotate(90)
        .align(
            center_x=extrusion.left - 2,
            center_y=stepper.front + 15,
            center_z=extrusion.center_z,
        )
    )

    side_bolt_back = (
        ~Bolt.M6(10)
        .slide(y=-20)
        .y_rotate(90)
        .align(
            center_x=extrusion.left - 2,
            center_y=stepper.back - 15,
            center_z=extrusion.center_z,
        )
    )

    top_bolt_front = (
        ~Bolt.M6(10)
        .top_to_bottom()
        .align(
            center_x=extrusion.center_x,
            center_y=side_bolt_front.center_y - 3,
            center_z=extrusion.top + 2,
        )
    )
    top_bolt_center = (
        ~Bolt.M6(10)
        .top_to_bottom()
        .align(
            center_x=extrusion.center_x,
            center_y=stepper.center_y,
            center_z=extrusion.top + 2,
        )
    )
    top_bolt_back = (
        ~Bolt.M6(10)
        .top_to_bottom()
        .align(
            center_x=extrusion.center_x,
            center_y=side_bolt_back.center_y + 3,
            center_z=extrusion.top + 2,
        )
    )

    rod_holder = Surface.free(
        Square(1, 1).align(left=rod.left - 18, front=extrusion.back + 2),
        Square(1, 1).align(left=rod.left, front=extrusion.back + 2),
        Circle(d=8).align(left=rod.left - 18, center_y=extrusion.back - 6),
        Circle(d=6).align(left=rod.left - 3, back=rod.back - 3),
        Circle(d=6).align(right=rod.right + 5, back=rod.back - 3),
        Circle(d=4).align(right=rod.right + 5, front=rod.front + 13),
    ).z_linear_extrude(bottom=stepper.top, distance=12)

    rod_holder_bolt = (
        ~Bolt.M3(20)
        .add_nut(-2, inline_clearance_size=20)
        .bottom_to_right()
        .align(
            right=rod_holder.right,
            center_y=rod.front + 5,
            center_z=rod_holder.center_z,
        )
    )
    rod_holder_clearance = ~Cube(2, 20, rod_holder.height + 1).align(
        center_x=rod.center_x, back=rod.front - 1, center_z=rod_holder.center_z
    )

    body = Volume(
        left=0,
        right=rod.left,
        back=extrusion.back + 2,
        front=extrusion.front - 2,
        top=extrusion.bottom,
        height=6,
    ).fillet_height(6, right=True, front=True)

    front_attachement = Volume(
        right=body.right,
        left=rod_holder.left,
        front=extrusion.back,
        depth=6,
        bottom=rod_holder.top - E,
        top=extrusion.top - 6,
    ).fillet_depth(8, top=True)
    front_bolt = (
        ~Bolt.M5(10)
        .bottom_to_back()
        .slide(z=20)
        .align(
            center_x=front_attachement.center_x,
            center_y=extrusion.back - 2,
            center_z=extrusion.center_z,
        )
    )

    bottom_right_bolt = ~Bolt.M6(10).align(
        center_x=body.right - 16,
        center_y=extrusion.center_y,
        center_z=extrusion.bottom - 2,
    )

    center_clearance = (
        ~Volume(
            center_x=stepper.center_x,
            width=30,
            front=body.front + E,
            depth=24,
            bottom=body.bottom - E,
            top=body.top + E,
        )
        .fillet_height(12, back=True)
        .reverse_fillet_front(12)
    )


if __name__ == "__main__":
    ZStepperMount().render_to_file(openscad=False)
    ZStepperMountWithZBackets().render_to_file()
